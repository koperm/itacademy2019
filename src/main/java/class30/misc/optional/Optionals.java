package class30.misc.optional;

import java.util.Optional;

public class Optionals {
    public static void main(String[] args) {
        // create
        Optional<Integer> emptyOpt = Optional.empty();
        Optional<Integer> opt2 = Optional.of(5);    // NPE if null value

        Optional<Integer> opt3 = Optional.ofNullable(null);
        Optional<Integer> opt4 = Optional.ofNullable(5);

        // ifPresent
        opt2.ifPresent(System.out::println);

        if (opt2.isPresent()) {
            System.out.println(opt2.get());
        }

        // orElse / orElseGet/ orElseThrow
        int value1 = emptyOpt.orElse(5);
        int value11 = emptyOpt.orElseGet(() -> 5);
//        int value2 = emptyOpt.orElseThrow(IllegalStateException::new);

        // filter
        int value3 = emptyOpt.filter(integer -> integer == 5)
                .orElse(10);

        int value4 = opt2.filter(integer -> integer == 6)
                .orElse(10);

        // map
        int value5 = emptyOpt.map(integer -> integer + 7)
                .orElse(10);

        // flatMap
        Optional<Optional<Integer>> optOfOpt2 = emptyOpt
                .map(integer -> Optional.ofNullable(integer));

        Optional<Integer> val = emptyOpt
                .flatMap(integer -> Optional.ofNullable(integer));
    }
}
