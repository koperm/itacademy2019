package class30.misc.classpath_jar;

import class30.misc.classpath_jar.pckg.SomeClass;
import com.google.common.base.Strings;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("These are my input arguments: " + Arrays.toString(args));
        Strings.nullToEmpty("");
        SomeClass.printSth();
    }
}
