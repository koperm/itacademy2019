package class30.misc.immutable;

import java.util.ArrayList;
import java.util.List;

public final class Person {
    private final String name;
    private final List<String> animals;

    public Person(String name, List<String> animals) {
        this.name = name;
        this.animals = new ArrayList<>(animals);    // TODO why?
    }

    public String getName() {
        return name;
    }

    public List<String> getAnimals() {
        return new ArrayList<>(animals);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", animals=" + animals +
                '}';
    }

    public static void main(String[] args) {
        Person person = new Person("Adam", List.of("dog", "cat"));
        System.out.println(person);
        List<String> animals = person.getAnimals();
//        animals.add("dragon");  // TODO why?
        System.out.println(person);

    }
}