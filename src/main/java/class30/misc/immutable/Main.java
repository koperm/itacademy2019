package class30.misc.immutable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> mutableList = Lists.newArrayList();
        List<String> immutableList1 = List.of("one", "two");
        List<String> immutableList2 = Collections.unmodifiableList(mutableList);
        List<String> immutableList3 = ImmutableList.of("one", "two");

        // TODO try to find immutable constructions for other data structures e.g. Set, Map
    }
}
