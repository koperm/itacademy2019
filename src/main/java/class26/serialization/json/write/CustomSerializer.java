package class26.serialization.json.write;

import class26.serialization.json.Car;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class CustomSerializer {

    public static void main(String[] args) throws IOException {
        SimpleModule module = new SimpleModule("CarSerializer", Version.unknownVersion());
        module.addSerializer(new CarSerializer(Car.class));

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(module);

        Car car = new Car("BMW", 4);
        String serializedCar = mapper.writeValueAsString(car);
        System.out.println(serializedCar);
    }

    private static class CarSerializer extends StdSerializer<Car> {
        protected CarSerializer(Class<Car> t) {
            super(t);
        }

        public void serialize(Car car, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("producer", car.getBrand());
            jsonGenerator.writeNumberField("doorCount", car.getDoors());
            jsonGenerator.writeEndObject();
        }
    }
}
