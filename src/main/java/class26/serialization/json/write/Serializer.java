package class26.serialization.json.write;

import class26.serialization.json.Car;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileOutputStream;
import java.io.IOException;

public class Serializer {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Car car = new Car("BMW", 4);
        objectMapper.writeValue(new FileOutputStream("output-2.json"), car);
        String json = objectMapper.writeValueAsString(car);
        System.out.println(json);
    }
}
