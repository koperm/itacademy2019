package class26.serialization.json.write;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConversion {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        String output = objectMapper.writeValueAsString(new Date());
        System.out.println(output);

        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        String output2 = objectMapper.writeValueAsString(new Date());
        System.out.println(output2);
    }
}
