package class26.serialization.json.read;

import class26.serialization.json.Car;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class CollectionsDeserializer {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = "{\"brand\":\"ford\"}, {\"brand\":\"Fiat\"}";

        Car[] carsFromArray = objectMapper.readValue(jsonString, Car[].class);
        System.out.println(carsFromArray);

        List<Car> carsFromList = objectMapper.readValue(jsonString, new TypeReference<List<Car>>() {});
        System.out.println(carsFromList);

        Map<String, Object> jsonMap = objectMapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {});
        System.out.println(jsonMap);
    }

}
