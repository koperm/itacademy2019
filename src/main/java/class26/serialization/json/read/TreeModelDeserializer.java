package class26.serialization.json.read;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class TreeModelDeserializer {
    public static void main(String[] args) throws IOException {
        String carJson =
                "{ " +
                        "\"brand\" : \"Mercedes\", " +
                        "\"doors\" : 5, " +
                        "\"owners\" : [\"John\", \"Jack\", \"Jill\"], " +
                        "\"nestedObject\" : { \"field\" : \"value\" } " +
                        "}";

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode jsonNode = objectMapper.readValue(carJson, JsonNode.class);
        JsonNode jsonNode1 = objectMapper.readTree(carJson);    // the same as above

        JsonNode brandNode = jsonNode.get("brand");
        String brand = brandNode.asText();
        System.out.println("brand = " + brand);

        JsonNode doorsNode = jsonNode.get("doors");
        int doors = doorsNode.asInt();
        System.out.println("doors = " + doors);

        JsonNode array = jsonNode.get("owners");
        JsonNode jsonNode2 = array.get(0);
        String john = jsonNode2.asText();
        System.out.println("john  = " + john);

        JsonNode child = jsonNode.get("nestedObject");
        JsonNode childField = child.get("field");
        String field = childField.asText();
        System.out.println("field = " + field);

    }
}