package class26.serialization.json.read;

import class26.serialization.json.Car;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;

public class CustomDeserializer {
    public static void main(String[] args) throws IOException {
        SimpleModule module = new SimpleModule("CarDeserializer", Version.unknownVersion());
        module.addDeserializer(Car.class, new CarDeserializer(Car.class));

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(module);

        String json = "{ \"brand\" : \"Ford\", \"doors\" : 6 }";
        Car car = mapper.readValue(json, Car.class);
        System.out.println(car);
    }

    private static class CarDeserializer extends StdDeserializer<Car> {

        public CarDeserializer(Class<Car> vc) {
            super(vc);
        }

        @Override
        public Car deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
            Car car = new Car();

            while (!parser.isClosed()) {
                JsonToken jsonToken = parser.nextToken();

                if (JsonToken.FIELD_NAME.equals(jsonToken)) {
                    String fieldName = parser.getCurrentName();
                    System.out.println(fieldName);


                    if ("brand".equals(fieldName)) {
                        car.setBrand(parser.getValueAsString());
                    } else if ("doors".equals(fieldName)) {
                        car.setDoors(parser.getValueAsInt());
                    }
                }
            }
            return car;
        }
    }
}
