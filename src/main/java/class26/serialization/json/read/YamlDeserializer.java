package class26.serialization.json.read;

import class26.serialization.json.Car;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;

public class YamlDeserializer {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        Car car = new Car("BMW", 4);
        String yamlString = objectMapper.writeValueAsString(car);
        Car deserializedCar = objectMapper.readValue(yamlString, Car.class);
        System.out.println(deserializedCar);
    }
}
