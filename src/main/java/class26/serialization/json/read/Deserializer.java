package class26.serialization.json.read;

import class26.serialization.json.Car;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Deserializer {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String carJson = "{ \"brand\" : \"Mercedes\", \"doors\" : 5 }";

        Car carFromString = objectMapper.readValue(carJson, Car.class);
        System.out.println(carFromString);

        Reader reader = new StringReader(carJson);
        Car carFromReader = objectMapper.readValue(reader, Car.class);
        System.out.println(carFromReader);

        File file = new File("output-2.json");
        Car carFromFile = objectMapper.readValue(file, Car.class);
        System.out.println(carFromFile);

        URL url = new URL("file:output-2.json");
        Car carFromUrl = objectMapper.readValue(url, Car.class);
        System.out.println(carFromUrl);

        InputStream inputStream = new FileInputStream("output-2.json");
        Car carFromInputStream = objectMapper.readValue(inputStream, Car.class);
        System.out.println(carFromInputStream);

        byte[] bytes = carJson.getBytes(StandardCharsets.UTF_8);
        Car carFromBytes = objectMapper.readValue(bytes, Car.class);
        System.out.println(carFromBytes);
    }
}
