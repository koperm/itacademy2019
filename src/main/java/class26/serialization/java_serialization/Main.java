package class26.serialization.java_serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String dir = "src\\main\\resources\\";
        String path = dir + "serializedPerson.bin";

        serialize(path);    // TODO try to run serialize and deserialize in separate executions, one after another
        deserialize(path);
    }

    private static void serialize(String path) throws IOException {
        Person person = new Person(45).setName("Joe").setHeight(180).setCountry("USA");
        System.out.println("Person:\t\t\t\t" + person);

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(person);
        }
    }

    private static void deserialize(String path) throws IOException, ClassNotFoundException {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(path))) {
            Person deserializedPerson = (Person) objectInputStream.readObject();
            System.out.println("DeserializedPerson:\t\t" + deserializedPerson);
        }

    }
}
