package class26.serialization.java_serialization;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Person implements Serializable {   // TODO try to remove Serializable
    private static final long serialVersionUID = 1L;
    static String country;
    private final int age;
    public String name;
    transient int height;

    public Person(int age) {
        this.age = age;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static String getCountry() {
        return country;
    }

    public Person setCountry(String country) {
        Person.country = country;
        return this;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Person setHeight(int height) {
        this.height = height;
        return this;
    }

    private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
        // perform the default de-serialization first
        in.defaultReadObject();

        country = in.readUTF();
//        age = in.readInt();   // TODO why
        country = in.readUTF();
        height = in.readInt();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        // perform the default serialization for all non-transient, non-static fields
        out.defaultWriteObject();

        out.writeUTF(country);
        out.writeInt(age);
        out.writeUTF(name);
        out.writeInt(height);
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name +
                ", height=" + height +
                ", country=" + country +
                ", serialVersionUID=" + serialVersionUID +
                '}';
    }
}

