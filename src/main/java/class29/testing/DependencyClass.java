package class29.testing;

public class DependencyClass {
    private String attr;

    public DependencyClass(String attr) {
        this.attr = attr;
    }

    public String getAttr() {
        return attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }
}
