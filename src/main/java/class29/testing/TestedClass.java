package class29.testing;

import class26.serialization.json.Car;

import java.util.List;

public class TestedClass {

    public String concatenate(String one, String two) {
        return one + two;
    }

    public List<String> getList() {
        return List.of("one", "two");
    }

    public String throwingFunction() {
        throw new RuntimeException("Throwing function failed");
    }

    public Car getCar() {
        return new Car();
    }

    public String methodWithDependency(DependencyClass dependencyClass) {
        String attr = dependencyClass.getAttr();
        dependencyClass.setAttr(attr);
        return attr;
    }
}
