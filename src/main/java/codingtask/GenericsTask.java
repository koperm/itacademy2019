package codingtask;

import java.util.List;

public class GenericsTask {
    public static void main(String[] args) {
        List<Float> floatList1 = List.of(2.0F);
        List<Float> floatList2 = List.of(4.0F);
        swapT(floatList1, floatList2);

        List<Integer> intList1 = List.of(2);
        List<Integer> intList2 = List.of(4);
        swapT(intList1, intList2);
    }

    static void swap(List<? extends Number> l1, List<? extends Number> l2) {
        Number t = l1.get(0);
//        l1.add(0, l2.get(0)); // FIXME
//        l2.add(t);    // FIXME
    }

    static <T extends Number> void swapT(List<T> l1, List<T> l2) {
        T t = l1.get(0);
        l1.add(0, l2.get(0));
        l2.add(t);
    }
}
