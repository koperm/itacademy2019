package codingtask;

public class AddArraysTask {

    public static void main(String[] args) {
        addArrays(null, null);
        addArrays(new int[]{1, 2, 3}, null);
        addArrays(null, new int[]{1, 2, 3});
        addArrays(new int[]{}, new int[]{});
        addArrays(new int[]{7, 7}, new int[]{});
        addArrays(new int[]{}, new int[]{7, 7});
        addArrays(new int[]{2, 3}, new int[]{4, 4});
        addArrays(new int[]{6, 6}, new int[]{4, 4});
        addArrays(new int[]{1, 2, 3}, new int[]{4, 4});
        addArrays(new int[]{9, 2, 3}, new int[]{8, 4});
        addArrays(new int[]{2, 3}, new int[]{1, 4, 4});
        addArrays(new int[]{2, 3}, new int[]{9, 8, 4});
        addArrays(new int[]{9, 9}, new int[]{1});

        sum(null, null);
        sum(new int[]{1, 2, 3}, null);
        sum(null, new int[]{1, 2, 3});
        sum(new int[]{}, new int[]{});
        sum(new int[]{7, 7}, new int[]{});
        sum(new int[]{}, new int[]{7, 7});
        sum(new int[]{2, 3}, new int[]{4, 4});
        sum(new int[]{6, 6}, new int[]{4, 4});
        sum(new int[]{1, 2, 3}, new int[]{4, 4});
        sum(new int[]{9, 2, 3}, new int[]{8, 4});
        sum(new int[]{2, 3}, new int[]{1, 4, 4});
        sum(new int[]{2, 3}, new int[]{9, 8, 4});
        sum(new int[]{9, 9}, new int[]{1});
    }

    private static int[] addArrays(int[] first, int[] second) {
        // validation

        int[] shorter, longer;
        if (first.length > second.length) {
            shorter = second;
            longer = first;
        } else {
            shorter = first;
            longer = second;
        }

        final int offset = longer.length - shorter.length;
        boolean overflow = false;
        int[] out = new int[longer.length];

        for (int i = longer.length - 1; i >= 0; i--) {
            int sum;
            if (i >= offset) {
                sum = longer[i] + shorter[i - offset];
            } else {
                sum = longer[i];
            }

            if (overflow) {
                sum++;
            }

            if (sum > 9) {
                overflow = true;
                out[i] = sum % 10;
            } else {
                overflow = false;
                out[i] = sum;
            }
        }

        return out;
    }

    public static int[] sum(int[] a, int[] b) {
        int lengthA = a.length;
        int lengthB = b.length;
        if (lengthA < lengthB) {
            return sum(b, a);
        }
        int[] sum = new int[lengthA];
        int carry = 0;
        int i = lengthA - 1;
        for (int j = lengthB - 1; j >= 0; i--, j--) {
            sum[i] = a[i] + b[j] + carry;
            carry = getCarry(sum, i);
        }
        for (; i >= 0; i--) {
            sum[i] = a[i] + carry;
            carry = getCarry(sum, i);
        }
        if (carry > 0) {
            int[] temp = new int[sum.length + 1];
            System.arraycopy(sum, 0, temp, 1, sum.length);
            temp[0] = 1;
            sum = temp;
        }
        return sum;
    }

    private static int getCarry(int[] sum, int i) {
        int carry;
        carry = 0;
        if (sum[i] > 9) {
            carry = 1;
            sum[i] -= 10;
        }
        return carry;
    }
}
