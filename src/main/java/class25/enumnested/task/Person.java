package class25.enumnested.task;

public class Person {
    private final String name;
    private final int basicDailyPay;
    private final WorkingDays workingDays;

    public Person(String name, int basicDailyPay, WorkingDays workingDays) {
        this.name = name;
        this.basicDailyPay = basicDailyPay;
        this.workingDays = workingDays;
    }

    public float calculateWeeklySalary() {
        int salary = 0;
        for (Day day : workingDays.getDays()) {
            salary += day.calculateDailyPay(basicDailyPay);
        }
        return salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", basicDailyPay=" + basicDailyPay +
                ", workingDays=" + workingDays +
                ", weeklySalary=" + calculateWeeklySalary() +
                '}';
    }
}
