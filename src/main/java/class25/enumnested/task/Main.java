package class25.enumnested.task;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        WorkingDays labourWorkingDays = new WorkingDays() {

            @Override
            public List<Day> getDays() {
                return List.of(Day.MONDAY, Day.TUESDAY, Day.WEDNESDAY, Day.THURSDAY, Day.FRIDAY);
            }

            @Override
            public String toString() {
                return printDays();
            }
        };

        WorkingDays emergencyWorkingDays = new EmergencyWorkingDays();

        Person doctor = new Person("Doctor", 1000, emergencyWorkingDays);
        Person mechanic = new Person("Mechanic", 500, labourWorkingDays);

        System.out.println(doctor);
        System.out.println(mechanic);

    }

    private static class EmergencyWorkingDays implements WorkingDays {

        @Override
        public List<Day> getDays() {
            return List.of(Day.values());
        }

        @Override
        public String toString() {
            return printDays();
        }
    }
}
