package class25.enumnested.nested.statics;

public class Outer {
    private int outerInt = 2;
    private static int outerStaticInt = 3;

    private void outerMethod() {
        System.out.println("outerMethod");
    }

    public static void outerStaticMethod() {
        System.out.println("outerStaticMethod");
    }

    static Insider getInsider() {
        return new Insider();
    }

    static class Insider {
        public int insiderInt = 23;

        public void nonStatic() {
//            localInt = outerInt;
//            outerMethod();
        }

        public static void main(String[] args) {
            int localInt = outerStaticInt;
//            localInt = outerInt;
//            outerMethod();
            outerStaticMethod();
            System.out.println("inside inner class Method");
        }
    }

}
