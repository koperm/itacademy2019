package class25.enumnested.nested.statics;

public class Main {
    public static void main(String[] args) {
        Outer.Insider insider = new Outer.Insider();

        Outer ou = new Outer();

        Outer.outerStaticMethod();
    }
}
