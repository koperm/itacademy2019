package class25.enumnested.nested.inner.local;

public class Outer {
    private int outerInt = 2;
    private static int outerStaticInt = 3;

    private static void outerStaticMethod() {
        System.out.println("outerStaticMethod");
    }

    private void outerMethodV2() {
        System.out.println("outerMethodV2");
    }

    void outerMethod() {
        System.out.println("inside outerMethod");

        class Inner {
//            private static int innerStaticInt = 3;
//
//            private static void innerStaticMethod() {
//                System.out.println("Illegal");
//            }

            void innerMethod() {
                int inner = outerInt;
                inner = outerStaticInt;
                outerStaticMethod();
                outerMethodV2();
                System.out.println("In the inner method");

            }
        }

        Inner inner = new Inner();
        inner.innerMethod();

        Inner inner2 = new Inner();
        inner2.innerMethod();
    }
}
