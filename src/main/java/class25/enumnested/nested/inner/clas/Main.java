package class25.enumnested.nested.inner.clas;

public class Main {
    public static void main(String[] args) {
        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();

        Outer.Inner innner2 = new Outer().new Inner();

        inner.innerMethod();
    }
}
