package class25.enumnested.nested.inner.clas;

class Outer {
    private int outerInt = 2;
    private static int outerStaticInt = 3;

    private void outerMethod() {
        System.out.println("outerMethod");
    }

    private static void outerStaticMethod() {
        System.out.println("outerStaticMethod");
    }

    class Inner {
        private int innerInt = 1;

//        private static int innerStaticInt = 3;    // TODO
//        private static void innerStaticMethod(){
//            System.out.println("Illegal");
//        }

        public void innerMethod() {
            int inner = outerInt;
            inner = outerStaticInt;
            outerStaticMethod();
            outerMethod();
            System.out.println(this.getClass().getSimpleName());
            System.out.println("In a static class method");
        }
    }
}