package class25.enumnested.nested.inner.anonymous;

public class ParentClass {
    protected int parentClassInt = 5;

    void doSomething() {
        System.out.println("parentClass doSomething");
    }
}
