package class25.enumnested.nested.inner.anonymous;

public class AnonymousInterface {

    static ParentInterface overridenParent = new ParentInterface() {
        @Override
        public void doSomething() {
            System.out.println("Overriden ParentInterface doSomething method");
        }
    };

    public static void main(String[] args) {
        overridenParent.doSomething();
//        ParentInterface parentInterface = new ParentInterface();
        ChildParentInterface childParentInterface = new ChildParentInterface();
    }

    private static class ChildParentInterface implements ParentInterface {

        @Override
        public void doSomething() {
            System.out.println("Overriden ParentInterface doSomething method");
        }
    }

}
