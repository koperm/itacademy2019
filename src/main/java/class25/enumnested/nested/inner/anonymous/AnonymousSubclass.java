package class25.enumnested.nested.inner.anonymous;

public class AnonymousSubclass {

    static ParentClass overridenParent = new ParentClass() {
        @Override
        void doSomething() {
            int local = parentClassInt;
            super.doSomething();
            System.out.println("Overriden ParentClass doSomething method");
        }
    };

    public static void main(String[] args) {
        ParentClass parentClass = new ParentClass();
        parentClass.doSomething();

        overridenParent.doSomething();
    }
}
