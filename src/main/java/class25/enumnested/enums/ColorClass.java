package class25.enumnested.enums;

import java.util.stream.Stream;

public class ColorClass {

    public static void main(String[] args) {
        Color enumColor = Color.BLUE;
        Color blue = Color.valueOf("BLUE");

        System.out.println(enumColor.name());

        System.out.println("\nAll enums");
        Stream.of(Color.values())
                .forEach(System.out::println);
        System.out.println();

        System.out.println(Color.valueOf("RED"));

        printMyFavouriteColor(enumColor);
    }

    public static void printMyFavouriteColor(Color color) {
        switch (color) {
            case GREEN:
            case BLUE:
            case RED:
                System.out.println("My favourite color is: " + color);
                break;
            default:
                System.out.println("Unknown color " + color);
        }
    }

    enum Color {
        GREEN,
        BLUE,
        RED
    }
}
