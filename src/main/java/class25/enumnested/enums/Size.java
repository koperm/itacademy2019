package class25.enumnested.enums;

public enum Size {
    SMALL,
    MEDIUM,
    LARGE   // No need semicolon if there is no body
}
