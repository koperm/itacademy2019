package class25.enumnested.enums;

public class SizeMain {
    public static void main(String[] args) {
        Clothes clothes = new Clothes(Size.LARGE);

        if (clothes.getSize() == Size.SMALL) {
            System.out.println("large size");
        }

    }

    static class Clothes {
        private Size size;

        Clothes(Size size) {
            this.size = size;
        }

        public Size getSize() {
            return size;
        }

    }
}
