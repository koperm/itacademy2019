package class25.enumnested.enums;

public class ColorClassWithParam {

    public static void main(String[] args) {
        Color blue = Color.BLUE;
//        Color fdasda = new Color("BLUE"); // TODO
        System.out.println(blue.getFirstLetter());
        System.out.println(blue);
    }

    enum Color {
        GREEN("G", 5), // new Color("G")
        BLUE("B"),
        RED("R");

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private static final String msg = "This class represent one of the most popular representation of visible colors";
        private final String firstLetter;

        Color(String letter) {
            this.firstLetter = letter;
        }

        Color(String letter, int size) {
            this.firstLetter = letter;
        }

        public String getFirstLetter() {
            return firstLetter;
        }

        @Override
        public String toString() {
            return msg;
        }
    }
}
