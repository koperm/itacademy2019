package class25.enumnested.enums;

public enum Operation {
    PLUS("+") {
        @Override
        public double apply(double x, double y) {
            return x + y;
        }
    },
    MINUS("-") {
        @Override
        public double apply(double x, double y) {
            return x - y;
        }
    },
    TIMES("*") {
        @Override
        public double apply(double x, double y) {
            return x * y;
        }
    },
    DIVIDE("/") {
        @Override
        public double apply(double x, double y) {
            return x / y;
        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private final String stringRepresentation;

    Operation(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }

    public abstract double apply(double x, double y);

    public static void main(String[] args) {
        System.out.println(Operation.PLUS.apply(3, 4));
        System.out.println(Operation.MINUS.apply(3, 4));
        System.out.println(Operation.TIMES.apply(3, 4));
        System.out.println(Operation.DIVIDE.apply(3, 4));
    }
}