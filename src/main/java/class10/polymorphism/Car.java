package class10.polymorphism;

public class Car {
    protected String fuel;
    private int tires;
    private static int bumpers = 4;

    private Car() {
    }

    protected Car(String fuel) {
        this();
        this.fuel = fuel;
    }

    protected void drive() {
        System.out.println("You consumed " + fuel + "gasolien type");
    }

    private void wash() {
        System.out.println("I am washing my car");
    }

    protected static void crash() {
        System.out.println("I am crashing my car");
        bumpers--;
    }

}
