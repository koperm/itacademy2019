package class10.polymorphism.subpckg;

import class10.polymorphism.Car;

public class Ferrari extends Car {
    private int tires;
    private static int bumpers = 4;

    public Ferrari() {
        this("LPG");
    }

    public Ferrari(String fuel) {
        super(fuel);
    }

    @Override
    protected void drive() {    // overridden version
        System.out.println("Ferrari took twice as much normal car of " + fuel); // fuel is protected. I have access to variable because of protected access specifier
        super.drive();
        this.drive(45);

        System.out.println("I have got " + tires + " tires");
    }

    void drive(int kmph) {  // overloaded version
        System.out.println("your velocity is " + kmph);
        super.drive();
    }

    private void wash() {
        System.out.println("I am washing my ferrari");
    }

    protected static void crash() {
        System.out.println("I am crashing my ferrari");
        bumpers--;
    }
}
