package class10.polymorphism.private_inheritance;

/*
    Taken from https://www.geeksforgeeks.org/overriding-in-java/
 */
class Base {
    private void fun() {
        System.out.println("Base fun");
    }
}

class Derived extends Base {
    private void fun() {
        System.out.println("Derived fun");
    }

    public static void main(String[] args) {
        Base obj = new Derived();
//        obj.fun();    // TODO why? Because private methods are not inherited
    }
}