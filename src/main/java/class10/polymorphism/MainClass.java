package class10.polymorphism;

import class10.polymorphism.subpckg.Ferrari;

public class MainClass {

    public static void main(String[] args) {
        Ferrari dieselFerrari = new Ferrari("Diesel");

        Ferrari lpgFerrari = new Ferrari("LPG");
        Car car = lpgFerrari;

        car.drive();    // which one? HINT: late binding (runtime binding)

        lpgFerrari.crash(); // which one? HING: Static uses early binding (compile time binding). Method will be chosen based on reference "lpgFerrari" or class "Ferrari" used to dereference the method "wash()"
        System.out.println(dieselFerrari);  // why destroying dieselFerrari lpgFerrari lost also a bumper?
        System.out.println(lpgFerrari);

        Ferrari.crash();    // the same as lpgFerrari.crash();

        car.crash();    // which one? static => compile time => based on reference or class type
        Car.crash();
    }
}
