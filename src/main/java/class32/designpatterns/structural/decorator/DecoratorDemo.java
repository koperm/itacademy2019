package class32.designpatterns.structural.decorator;

public class DecoratorDemo {

    public static void main(String[] args) {
        System.out.println("\nZip the File and then encrypt\n");

        Decorator.FileReader file = new Decorator.FileReader();
        // Zip the File
        Decorator.ZipReader zip = new Decorator.ZipReader(file);
        // Encrypt the zip file
        Decorator.EncryptedReader enc = new Decorator.EncryptedReader(zip);

        enc.Read();

        System.out.println("\nEncrypt the file and then zip it\n");

        // Encrypt the file
        enc = new Decorator.EncryptedReader(file);
        // Zip the encrypted file
        zip = new Decorator.ZipReader(enc);

        zip.Read();
    }
}
