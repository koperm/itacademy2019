package class32.designpatterns.structural.decorator;

class Decorator {

    static abstract class IReader {
        abstract public void Read();
    }

    static class FileReader extends IReader {
        @Override
        public void Read() {
            System.out.println("File reader - Read");
        }
    }

    static class ReaderDecorator extends IReader {
        IReader reader;

        public ReaderDecorator(IReader rd) {
            this.reader = rd;
        }

        @Override
        public void Read() {
            reader.Read();
        }
    }

    static class ZipReader extends ReaderDecorator {
        public ZipReader(IReader rd) {
            super(rd);
        }

        @Override
        public void Read() {
            System.out.println("ZipReader - Reading");
            super.Read();
        }
    }

    static class EncryptedReader extends ReaderDecorator {
        public EncryptedReader(IReader rd) {
            super(rd);
        }

        @Override
        public void Read() {
            System.out.println("EncryptedReader - Reading");
            super.Read();
        }
    }
}
