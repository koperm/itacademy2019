package class32.designpatterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

class Flyweight {

    static abstract class IShape {
        public abstract void print();
    }

    static class Rectangle extends IShape {
        @Override
        public void print() {
            System.out.println("Printing Rectangle");
        }
    }

    static class Circle extends IShape {
        @Override
        public void print() {
            System.out.println("Printing Circle");
        }
    }

    static class ShapeObjectFactory {
        Map<String, IShape> shapes = new HashMap<>();

        public int getTotalObjectsCreated() {
            return shapes.size();

        }

        public IShape getShape(String shapeName) throws Exception {
            IShape shape = null;
            if (shapes.containsKey(shapeName)) {
                shape = shapes.get(shapeName);
            } else {
                switch (shapeName) {
                    case "Rectangle":
                        shape = new Rectangle();
                        shapes.put("Rectangle", shape);
                        break;
                    case "Circle":
                        shape = new Circle();
                        shapes.put("Circle", shape);
                        break;
                    default:
                        throw new Exception("Factory cannot create the object specified");
                }
            }
            return shape;
        }

    }

}
