package class32.designpatterns.structural.flyweight;

class FlyweightDemo {

    public static void main(String[] args) throws Exception {
        Flyweight.ShapeObjectFactory sof = new Flyweight.ShapeObjectFactory();

        Flyweight.IShape shape = sof.getShape("Rectangle");
        shape.print();
        shape = sof.getShape("Circle");
        shape.print();
        shape = sof.getShape("Circle");
        shape.print();
        shape = sof.getShape("Circle");
        shape.print();

        int numObjs = sof.getTotalObjectsCreated();
        System.out.println("Total Shapes created = " + numObjs);
    }
}
