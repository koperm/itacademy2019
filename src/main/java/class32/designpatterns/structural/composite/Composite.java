package class32.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

class Composite {

    static abstract class Graphic {
        //Virtual function that prints the graphic.
        public abstract void Print();
    }

    static class CompositeGraphic extends Graphic {
        //Collection of child graphics.
        List<Graphic> mChildGraphics = new ArrayList<>();

        //Prints the graphic by Delegating to its Child
        @Override
        public void Print() {
            for (Graphic graphic : mChildGraphics) {
                graphic.Print();
            }
        }

        //Adds the graphic to the composition.
        public void add(Graphic graphic) {
            mChildGraphics.add(graphic);
        }

        //Removes the graphic from the composition.
        public void Remove(Graphic graphic) {
            mChildGraphics.remove(graphic);
        }
    }

    static class Ellipse extends Graphic {
        //Prints the graphic.
        @Override
        public void Print() {
            System.out.println("Ellipse");
        }
    }
}
