package class32.designpatterns.structural.composite;

class CompositeDemo {

    public static void main(String[] args) {
        //Initialize four ellipses
        Composite.Ellipse ellipse1 = new Composite.Ellipse();
        Composite.Ellipse ellipse2 = new Composite.Ellipse();
        Composite.Ellipse ellipse3 = new Composite.Ellipse();
        Composite.Ellipse ellipse4 = new Composite.Ellipse();

        //Initialize three composite graphics
        Composite.CompositeGraphic graphic = new Composite.CompositeGraphic();
        Composite.CompositeGraphic graphic1 = new Composite.CompositeGraphic();
        Composite.CompositeGraphic graphic2 = new Composite.CompositeGraphic();

        //Composes the graphics
        graphic1.add(ellipse1);
        graphic1.add(ellipse2);
        graphic1.add(ellipse3);

        graphic2.add(ellipse4);

        graphic.add(graphic1);
        graphic.add(graphic2);

        //Prints the complete graphic (four times the string "Ellipse").
        graphic.Print();
    }

}
