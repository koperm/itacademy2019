package class32.designpatterns.structural.bridge;

class Bridge {

    static abstract class Window {
        private WindowImp wndImp;
        private boolean isPlatFormX = false;
        private boolean isPlatFormMac = true;

        public Window() {
            if (this.isPlatFormX) {
                wndImp = new XWindowImp();
            } else if (this.isPlatFormMac) {
                wndImp = new MacWindowImp();
            }
        }

        public void DrawRect() {
            wndImp.DrawLine("Left");
            wndImp.DrawLine("Top");
            wndImp.DrawLine("Right");
            wndImp.DrawLine("Bottom");
        }
    }

    static class IconWindow extends Window {
        @Override
        public void DrawRect() {
            // Single rectangle
            super.DrawRect();

        }
    }

    static class DialogWindow extends Window {
        @Override
        public void DrawRect() {
            // double rectangle
            super.DrawRect();
            super.DrawRect();
        }
    }

    static abstract class WindowImp {
        public abstract void DrawLine(String side);
    }

    static class XWindowImp extends WindowImp {
        @Override
        public void DrawLine(String side) {
            System.out.println("XWindowImp DrawLine >> " + side);
        }
    }

    static class MacWindowImp extends WindowImp {
        @Override
        public void DrawLine(String side) {
            System.out.println("MacWindowImp DrawLine >> " + side);
        }
    }
}
