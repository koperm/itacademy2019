package class32.designpatterns.structural.bridge;

class BridgeDemo {

    public static void main(String[] args) {
        Bridge.Window w = null;

        System.out.println("DrawRect for IconWindow");
        w = new Bridge.IconWindow();
        w.DrawRect();

        System.out.println();
        System.out.println("DrawRect for DialogWindow");
        w = new Bridge.DialogWindow();
        w.DrawRect();
    }
}
