package class32.designpatterns.structural.proxy;

class Proxy {

    static abstract class Client {
        abstract public int getData();
    }

    static class RealHeavyClient extends Client {
        public RealHeavyClient() {
            System.out.println("RealHeavyClient extends Client");
        }

        @Override
        public int getData() {
            // very heavy and complex operation
            return 1234;
        }
    }

    static class ProxyLoggingClient extends Client {
        Client client;

        public ProxyLoggingClient() {
            System.out.println("ProxyLoggingClient extends Client");
            client = new RealHeavyClient();
        }

        @Override
        public int getData() {
            System.out.println("Some extending logging");
            int data = client.getData();
            System.out.println("Some extending logging");
            return data;
        }
    }
}
