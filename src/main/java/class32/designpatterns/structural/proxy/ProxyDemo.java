package class32.designpatterns.structural.proxy;

public class ProxyDemo {

    public static void main(String[] args) {
        Proxy.Client proxy = new Proxy.ProxyLoggingClient();
        System.out.println("Data from Real / Proxy = " + proxy.getData());
    }
}
