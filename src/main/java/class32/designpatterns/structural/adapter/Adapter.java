package class32.designpatterns.structural.adapter;

public class Adapter {

    abstract static class MyCalcInt {
        abstract public int add(int a, int b);
    }

    abstract static class ThirdPartyCalcInt {
        abstract public float add(String a, String b);
    }

    static class ThirdPartyCalculator extends ThirdPartyCalcInt {
        @Override
        public float add(String a, String b) {
            return Float.valueOf(a) + Float.valueOf(b);
        }
    }

    static class MyPrimitiveCalc extends MyCalcInt {

        @Override
        public int add(int a, int b) {
            return a + b;
        }
    }

    static class CalcAdapter extends MyCalcInt {
        private ThirdPartyCalculator calc;

        public CalcAdapter() {
            calc = new ThirdPartyCalculator();
        }

        @Override
        public int add(int a, int b) {
            return (int) calc.add(String.valueOf(a), String.valueOf(b));
        }
    }
}
