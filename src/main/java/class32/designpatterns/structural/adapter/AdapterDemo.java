package class32.designpatterns.structural.adapter;

class AdapterDemo {

    public static void main(String[] args) {
        // Old code
        Adapter.MyCalcInt calc1 = new Adapter.MyPrimitiveCalc();
        calc1.add(10, 20);

        // My new third party calculator is not backward compatible
        Adapter.ThirdPartyCalculator calc = new Adapter.ThirdPartyCalculator();
        calc.add("10", "10");

        // Adapter converts client format to format understood by calculator implementation
        calc1 = new Adapter.CalcAdapter();
        calc1.add(10, 20);
    }
}
