package class32.designpatterns.structural.facade;

class FacadeDemo {
    public static void main(String[] args) {
        Facade.CarFacade facade = new Facade.CarFacade();
        facade.createCompleteCar();
    }
}
