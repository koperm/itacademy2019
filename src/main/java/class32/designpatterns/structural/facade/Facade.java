package class32.designpatterns.structural.facade;

class Facade {

    static class CarBody {
        public void addCarBody() {
            System.out.println("  >> CarBody addCarBody");
        }
    }

    static class WheelSystem {
        public void addWheelSystem() {
            System.out.println("  >> WheelSystem addWheelSystem");
        }
    }

    static class ChassisSystem {
        public void addChassisSystem() {
            System.out.println("  >> ChassisSystem addChassisSystem");
        }
    }

    static class SteeringSystem {
        public void addSteeringSystem() {
            System.out.println("  >> SteeringSystem addSteeringSystem");
        }
    }

    static class CarFacade {
        CarBody car;
        WheelSystem wheel;
        ChassisSystem chassis;
        SteeringSystem steering;

        public CarFacade() {
            car = new CarBody();
            wheel = new WheelSystem();
            chassis = new ChassisSystem();
            steering = new SteeringSystem();
        }

        public void createCompleteCar() {
            System.out.println("Creating Car");
            car.addCarBody();
            wheel.addWheelSystem();
            chassis.addChassisSystem();
            steering.addSteeringSystem();
            System.out.println("Car creation complete");
        }
    }
}
