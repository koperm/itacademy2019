package class32.designpatterns.behavioral.observer;

class ObserverDemo {
    public static void main(String[] args) {
        Observer.XMLDoc doc = new Observer.XMLDoc();
        Observer.ChartView view1 = new Observer.ChartView(doc);
        Observer.GraphView view2 = new Observer.GraphView(doc);
        doc.setData(100);
        doc.setData(200);
    }
}
