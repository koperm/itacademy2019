package class32.designpatterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

class Observer {
    static abstract class Subject {
        abstract public void attach(AObserver observer);

        abstract public void detach(AObserver observer);

        abstract public void nottify();
    }

    static class Document extends Subject {
        private List<AObserver> observerList = new ArrayList<>();

        // ISubject Members region

        @Override
        public void attach(AObserver observer) {
            observerList.add(observer);
        }

        @Override
        public void detach(AObserver observer) {
            observerList.remove(observer);
        }

        @Override
        public void nottify() {
            for (AObserver observer : observerList) {
                observer.update();
            }
        }

        // ISubject Members region end

    }

    static class XMLDoc extends Document {
        private Object data = null;

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
            nottify();
        }

    }

    static abstract class AObserver {
        abstract public void update();
    }

    static class GraphView extends AObserver {
        private XMLDoc sub = null;

        public GraphView(XMLDoc sub) {
            this.sub = sub;
            sub.attach(this);
        }

        @Override
        public void update() {
            System.out.println(this.toString() + "->" + sub.getData());
        }

    }

    static class ChartView extends AObserver {
        private XMLDoc sub = null;

        public ChartView(XMLDoc sub) {
            this.sub = sub;
            sub.attach(this);
        }

        @Override
        public void update() {
            System.out.println(this.toString() + "->" + sub.getData());
        }
    }
}
