package class32.designpatterns.behavioral.visitor;

class VisitorDemo {
    public static void main(String[] args) {
        Visitor.Circle circle = new Visitor.Circle();
        Visitor.Trangle triangle = new Visitor.Trangle();

        Visitor.DrawVisitor visitor = new Visitor.DrawVisitor();

        circle.accept(visitor);
        triangle.accept(visitor);

        Visitor.ScaleVisitor anotherVisitor = new Visitor.ScaleVisitor();

        circle.accept(anotherVisitor);
        triangle.accept(anotherVisitor);
    }
}
