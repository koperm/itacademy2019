package class32.designpatterns.behavioral.visitor;

class Visitor {

    static abstract class AVisitor {
        abstract public void visitCircle(Circle circle);

        abstract public void visitTrangle(Trangle trangle);
    }

    static abstract class Shape {
        abstract public void accept(AVisitor visitor);
    }

    static class Circle extends Shape {
        @Override
        public void accept(AVisitor visitor) {
            visitor.visitCircle(this);
        }
    }

    static class Trangle extends Shape {
        @Override
        public void accept(AVisitor visitor) {
            visitor.visitTrangle(this);
        }
    }

    static class DrawVisitor extends AVisitor {
        @Override
        public void visitCircle(Circle circle) {
            System.out.println("Drawing Circle");
        }

        @Override
        public void visitTrangle(Trangle trangle) {
            System.out.println("Drawing Trangle");
        }
    }

    static class ScaleVisitor extends AVisitor {
        @Override
        public void visitCircle(Circle circle) {
            System.out.println("Scaling Circle");
        }

        @Override
        public void visitTrangle(Trangle trangle) {
            System.out.println("Scaling Trangle");
        }
    }
}
