package class32.designpatterns.behavioral.memento;

class MementoDemo {
    public static void main(String[] args) {
        Memento.FormInputValues frm = new Memento.FormInputValues();
        frm.setState("Switched on"); // Some input done by user

        frm.saveMemento();

        // Continue with the update as we have saved the memento
        frm.setState("Switched off");

        // Restore the saved state
        frm.restoreMemento();
    }
}
