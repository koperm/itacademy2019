package class32.designpatterns.behavioral.memento;

class Memento {

    static class FormInputValues {
        private MementoImpl memento;
        private String state;

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
            System.out.println("State is " + state);
        }

        public MementoImpl saveMemento() {
            memento = new MementoImpl();
            memento.state = state;
            return memento;
        }

        public void restoreMemento() {
            System.out.println("Restoring the state. Actual state is " + this.memento.state);
            this.state = this.memento.state;
        }
    }

    /// <summary>
    /// Holds data for the input values.
    /// </summary>
    static class MementoImpl {
        private String state;

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

    }

    // "Caretaker"
    static class BackUpFormInputValues {
        private MementoImpl memento;

        public MementoImpl getMemento() {
            return memento;
        }

        public void setMemento(MementoImpl memento) {
            this.memento = memento;
        }

    }
}
