package class32.designpatterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

class Iterator {

    // "Aggregate"
    static abstract class IList {
        public abstract AbsIterator CreateIterator();
    }

    // "ConcreteAggregate"

    static class ConcreteList extends IList {
        private List items = new ArrayList();

        @Override
        public AbsIterator CreateIterator() {
            return new BackwardIterator(this);
        }

        public int getCount() {
            return items.size();
        }

        public Object get(int index) {
            return items.get(index);
        }

        public void add(Object item) {
            items.add(item);
        }

    }

    // "Iterator"

    static abstract class AbsIterator {
        public abstract Object first();

        public abstract Object next();
    }

    static class ForwardIterator extends AbsIterator {
        private ConcreteList aggregate;
        private int current = 0;

        // Constructor
        public ForwardIterator(ConcreteList aggregate) {
            this.aggregate = aggregate;
        }

        @Override
        public Object first() {
            return aggregate.get(0);
        }

        @Override
        public Object next() {
            Object ret = null;
            if (current < aggregate.getCount() - 1) {
                ret = aggregate.get(++current);
            }
            return ret;
        }

    }

    static class BackwardIterator extends AbsIterator {
        private ConcreteList aggregate;
        private int current = 0;

        // Constructor
        public BackwardIterator(ConcreteList aggregate) {
            this.aggregate = aggregate;
            this.current = aggregate.getCount() - 1;
        }

        @Override
        public Object first() {
            return aggregate.get(this.aggregate.getCount() - 1);
        }

        @Override
        public Object next() {
            Object ret = null;
            if (current > 0) {
                ret = aggregate.get(--current);
            }
            return ret;
        }

    }
}
