package class32.designpatterns.behavioral.iterator;

class IteratorDemo {
    public static void main(String[] args) {
        Iterator.ConcreteList a = new Iterator.ConcreteList();
        a.add("Item A");
        a.add("Item B");
        a.add("Item C");
        a.add("Item D");

        // Create Iterator and provide List
        Iterator.BackwardIterator back = new Iterator.BackwardIterator(a);
        Object item = null;
        System.out.println("Iterating over collection: with backward iterator");

        item = back.first();
        while (item != null) {
            System.out.println(item);
            item = back.next();
        }
        System.out.println();
        // Create Iterator and provide List
        Iterator.ForwardIterator forward = new Iterator.ForwardIterator(a);

        System.out.println("Iterating over collection: with forward iterator");

        item = forward.first();
        while (item != null) {
            System.out.println(item);
            item = forward.next();
        }
    }
}
