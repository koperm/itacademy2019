package class32.designpatterns.behavioral.state;

class State {
    static abstract class PrinterState {
        abstract public void handle(Printer context);

        /*virtual*/
        public void cancel(Printer context) {
        }
    }

    static class StateReady extends PrinterState {
        @Override
        public void handle(Printer context) {
            System.out.println("Print Started");
            context.setState(new StatePrintStart());
        }
    }

    static class StatePrintStart extends PrinterState {
        @Override
        public void handle(Printer context) {
            System.out.println("Ready to print");
            context.setState(new StatePrinting());
        }

        @Override
        public void cancel(Printer context) {
            System.out.println("Canceling ...");
            context.setState(new StatePrintEnd());
        }
    }

    static class StatePrinting extends PrinterState {
        @Override
        public void handle(Printer context) {
            System.out.println("Printing ...");
            context.setState(new StatePrintEnd());
        }

        @Override
        public void cancel(Printer context) {
            System.out.println("Canceling ...");
            context.setState(new StatePrintEnd());
        }
    }

    static class StatePrintEnd extends PrinterState {
        @Override
        public void handle(Printer context) {
            System.out.println("Print Ended \n");
            context.setState(new StateReady());
        }
    }
    // "Context"

    static class Printer {
        private PrinterState state;

        // Constructor
        public Printer(PrinterState state) {
            this.state = state;
        }

        public PrinterState getState() {
            return state;
        }

        public void setState(PrinterState state) {
            this.state = state;
            //System.out.println("Stateextends " +
            //  state.GetType().Name);
        }

        public void print() {
            state.handle(this);
        }

        public void cancel() {
            state.cancel(this);
            state.handle(this);
        }
    }
}
