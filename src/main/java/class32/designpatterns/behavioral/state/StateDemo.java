package class32.designpatterns.behavioral.state;

class StateDemo {
    public static void main(String[] args) {

        // Setup context in a state
        State.Printer c = new State.Printer(new State.StateReady());

        // Complete print job
        c.print();
        c.print();
        c.print();
        c.print();

        c = new State.Printer(new State.StateReady());

        // Cancel after start
        c.print();
        c.cancel();

        c = new State.Printer(new State.StateReady());
        // Cancel in printing
        c.print();
        c.print();
        c.cancel();
    }
}
