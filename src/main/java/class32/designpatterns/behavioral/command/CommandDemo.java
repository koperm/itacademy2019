package class32.designpatterns.behavioral.command;

class CommandDemo {
    public static void main(String[] args) {
        Command.FileViewer viewer = new Command.FileViewer();
        Command.FileOpenCommand command = new Command.FileOpenCommand(viewer);
        Command.InvokeCommand invoke = new Command.InvokeCommand(command);
        invoke.executeCommand();
    }
}
