package class32.designpatterns.behavioral.command;

class Command {

    static abstract class AbstractCommand {
        abstract public void execute();
    }

    static class FileOpenCommand extends AbstractCommand {
        protected FileViewer fileViewer;

        public FileOpenCommand(FileViewer viewer) {
            this.fileViewer = viewer;
        }

        @Override
        public void execute() {
            fileViewer.showFile();
        }
    }

    static class FileViewer {
        public void showFile() {
            System.out.println("FileViewer ShowFile");
        }
    }

    static class InvokeCommand {
        private AbstractCommand command;

        public InvokeCommand(AbstractCommand command) {
            this.command = command;
        }

        public void executeCommand() {
            command.execute();
        }
    }
}
