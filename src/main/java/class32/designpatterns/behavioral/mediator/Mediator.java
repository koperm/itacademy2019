package class32.designpatterns.behavioral.mediator;

class Mediator {

    static abstract class AMediator {
        public abstract void send(String message, Control colleague);
    }

    static class DialogMediator extends AMediator {
        private ListControl listCtrl;
        private ButtonControl btnCtrl;

        public ListControl getListCtrl() {
            return listCtrl;
        }

        public ButtonControl getBtnCtrl() {
            return btnCtrl;
        }

        public void setListCtrl(ListControl value) {
            listCtrl = value;
        }

        public void setBtnCtrl(ButtonControl value) {
            btnCtrl = value;
        }

        @Override
        public void send(String message, Control colleague) {
            if (colleague == listCtrl) {
                btnCtrl.notify(message);
            } else {
                listCtrl.notify(message);
            }
        }
    }

    static abstract class Control {
        protected AMediator mediator;

        // Constructor
        public Control(AMediator mediator) {
            this.mediator = mediator;
        }
    }

    static class ListControl extends Control {
        // Constructor
        public ListControl(AMediator mediator) {
            super(mediator);
        }

        public void send(String message) {
            mediator.send(message, this);
        }

        public void notify(String message) {
            System.out.println("ListControl gets messageextends " + message);
        }
    }

    static class ButtonControl extends Control {
        // Constructor
        public ButtonControl(AMediator mediator) {
            super(mediator);
        }


        public void send(String message) {
            mediator.send(message, this);
        }

        public void notify(String message) {
            System.out.println("ButtonControl gets messageextends "
                    + message);
        }
    }
}
