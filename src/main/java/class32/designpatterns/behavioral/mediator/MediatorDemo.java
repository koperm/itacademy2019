package class32.designpatterns.behavioral.mediator;

class MediatorDemo {
    public static void main(String[] args) {
        Mediator.DialogMediator dialogMediator = new Mediator.DialogMediator();

        Mediator.ListControl listCtrl = new Mediator.ListControl(dialogMediator);
        Mediator.ButtonControl btnCtrl = new Mediator.ButtonControl(dialogMediator);

        dialogMediator.setListCtrl(listCtrl);
        dialogMediator.setBtnCtrl(btnCtrl);

        listCtrl.send("List Control is here");
        btnCtrl.send("Button Control is here");
    }
}
