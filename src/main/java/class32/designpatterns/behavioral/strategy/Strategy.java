package class32.designpatterns.behavioral.strategy;

class Strategy {
    static class Invoice {
        double tot;
        NetPayable calc;

        public void setNetPayableCalculator(NetPayable calc) {
            this.calc = calc;
        }

        public double getGrossAmount() {
            return tot;
        }

        public void setGrossAmmount(double tot) {
            this.tot = tot;
        }

        public double getNetAmount() throws Exception {
            if (this.calc != null) {
                return this.calc.calculateTotal(this);
            } else {
                throw new Exception("NetPayableCalculator is not assigned");
            }
        }

    }

    static abstract class NetPayable {
        abstract public double calculateTotal(Invoice inv);
    }

    static class LocalPayable extends NetPayable {
        @Override
        public double calculateTotal(Invoice inv) {
            return inv.getGrossAmount() + 0.04 * inv.getGrossAmount();
        }
    }

    static class InternationalPayable extends NetPayable {
        @Override
        public double calculateTotal(Invoice inv) {
            return inv.getGrossAmount() + 0.10 * inv.getGrossAmount();
        }
    }
}
