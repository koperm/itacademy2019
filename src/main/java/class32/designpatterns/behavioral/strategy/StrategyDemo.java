package class32.designpatterns.behavioral.strategy;

class StrategyDemo {
    public static void main(String[] args) throws Exception {
        Strategy.Invoice inv = new Strategy.Invoice();

        inv.setGrossAmmount(100);
        inv.setNetPayableCalculator(new Strategy.LocalPayable());
        System.out.println("When Calculator is Local");
        System.out.println(inv.getNetAmount());
        inv.setNetPayableCalculator(new Strategy.InternationalPayable());
        System.out.println("When Calculator is International");
        System.out.println(inv.getNetAmount());
    }
}
