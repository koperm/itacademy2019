package class32.designpatterns.behavioral.chainOfResp;

class ChainOfResponsibilityDemo {
    public static void main(String[] args) {
        // Setup Chain of Responsibility
        ChainOfResponsibility.Approver to = new ChainOfResponsibility.TellerOperator();
        ChainOfResponsibility.Approver abm = new ChainOfResponsibility.AsstBranchManager();
        ChainOfResponsibility.Approver bm = new ChainOfResponsibility.BranchManager();
        to.setSuccessor(abm);
        abm.setSuccessor(bm);

        // Generate and process request
        int[] requests = {2000, 5000, 14000, 22000, 180000, 30000, 20000};

        for (int request : requests) {
            to.approve(request);
        }
    }
}
