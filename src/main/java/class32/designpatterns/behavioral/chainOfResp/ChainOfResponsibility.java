package class32.designpatterns.behavioral.chainOfResp;

class ChainOfResponsibility {

    static abstract class Approver {
        protected Approver successor;

        public void setSuccessor(Approver successor) {
            this.successor = successor;
        }

        public abstract void approve(int request);
    }

    static class TellerOperator extends Approver {
        @Override
        public void approve(int request) {
            if (request >= 0 && request < 10000) {
                System.out.println(this.getClass().getSimpleName() + " handled request " + request);
            } else if (successor != null) {
                successor.approve(request);
            }
        }
    }

    static class AsstBranchManager extends Approver {
        @Override
        public void approve(int request) {
            if (request >= 10000 && request < 100000) {
                System.out.println(this.getClass().getSimpleName() + " handled request " + request);
            } else if (successor != null) {
                successor.approve(request);
            }
        }
    }

    static class BranchManager extends Approver {
        @Override
        public void approve(int request) {
            if (request >= 100000 && request < 1000000) {
                System.out.println(this.getClass().getSimpleName() + " handled request " + request);
            } else {
                System.out.println("This branch cannot approve your request. Contact head Office");
            }
        }
    }
}
