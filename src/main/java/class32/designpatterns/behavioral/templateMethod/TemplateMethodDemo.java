package class32.designpatterns.behavioral.templateMethod;

class TemplateMethodDemo {
    public static void main(String[] args) {
        TemplateMethod.Document xml = new TemplateMethod.XMLDocument();
        TemplateMethod.Document html = new TemplateMethod.HTMLDocument();
        xml.Print();
        html.Print();
    }
}
