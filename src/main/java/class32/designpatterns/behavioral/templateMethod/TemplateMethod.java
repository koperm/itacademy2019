package class32.designpatterns.behavioral.templateMethod;

class TemplateMethod {
    static abstract class Document {
        public abstract void PrintHeader();

        public abstract void PrintBody();

        public abstract void PrintFooter();

        public void Print() {
            // Print algo is to print header body and then footer
            PrintHeader();
            PrintBody();
            PrintFooter();
        }
    }


    static class XMLDocument extends Document {
        @Override
        public void PrintHeader() {
            System.out.println("XMLDocument.PrintHeader()");
        }

        @Override
        public void PrintBody() {
            System.out.println("XMLDocument.PrintBody()");
        }

        @Override
        public void PrintFooter() {
            System.out.println("XMLDocument.PrintFooter()");
        }
    }

    static class HTMLDocument extends Document {
        @Override
        public void PrintHeader() {
            System.out.println("HTMLDocument.PrintHeader()");
        }

        @Override
        public void PrintBody() {
            System.out.println("HTMLDocument.PrintBody()");
        }

        @Override
        public void PrintFooter() {
            System.out.println("HTMLDocument.PrintFooter()");
        }
    }
}
