package class32.designpatterns.creational.factory;

// Factory or Factory method
class Factory {

    static abstract class Processor {
        abstract int getMHz();
    }

    static class ConcreteProcessor extends Processor {
        private int mHz = 1024;

        @Override
        public int getMHz() {
            return mHz;
        }
    }

    static abstract class ComputerFactory {
        public abstract Processor getProcessor();
    }

    static class ConcreteComputerFactory extends ComputerFactory {
        ConcreteProcessor concProc = null;

        @Override
        public Processor getProcessor() {
            System.out.println("ConcreteComputerFactory getProcessor() ");

            if (null == concProc) {
                System.out.println("Creating ConcreteProcessor ...");
                concProc = new ConcreteProcessor();
            }
            return concProc;
        }
    }
}
