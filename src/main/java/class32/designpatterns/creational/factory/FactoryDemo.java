package class32.designpatterns.creational.factory;

class FactoryDemo {

    public static void main(String[] args) {
        // Assemble the Computer
        System.out.println("Creating ConcreteComputerFactory ...");
        Factory.ComputerFactory factory = new Factory.ConcreteComputerFactory();

        System.out.println("Getting Processor ...");
        Factory.Processor computer = factory.getProcessor();

        System.out.println("Assembled PC running at " + computer.getMHz() + "MHz");
    }
}

