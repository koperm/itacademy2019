package class32.designpatterns.creational.abstractFactory;

class AbstractFactoryDemo {

    public static void main(String[] args) {
        AbstractFactory.AbstractGUIFactory factory = AbstractFactory.AbstractGUIFactory.getFactory(AbstractFactory.AbstractGUIFactory.OS_TYPE.MAC);
        AbstractFactory.Control button = factory.createButton();
        button.show();
        AbstractFactory.Control label = factory.CreateLabel();
        label.show();
    }
}
