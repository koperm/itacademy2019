package class32.designpatterns.creational.abstractFactory;

class AbstractFactory {
    static abstract class AbstractGUIFactory {

        public static AbstractGUIFactory getFactory(OS_TYPE osType) {
            if (OS_TYPE.MAC == osType) {
                return new MacFactory();
            } else if (OS_TYPE.WINDOWS == osType) {
                return new WinFactory();
            } else {
                throw new IllegalArgumentException("Invalid osType = " + osType);
            }
        }

        public abstract Control createButton();

        public abstract Control CreateLabel();

        public enum OS_TYPE {
            MAC,
            WINDOWS
        }
    }

    static class WinFactory extends AbstractGUIFactory {

        //  Its there just to have the static classdiagram going
        private Control btn = new WinButton();
        private Control lbl = new WinLabel();

        @Override
        public Control createButton() {
            return this.btn;
        }

        @Override
        public Control CreateLabel() {
            return this.lbl;
        }
    }

    static class MacFactory extends AbstractGUIFactory {

        //  Its there just to have the static classdiagram going
        private Control btn = new MacButton();
        private Control lbl = new MacLabel();

        @Override
        public Control createButton() {
            return this.btn;
        }

        @Override
        public Control CreateLabel() {
            return this.lbl;
        }
    }

    static abstract class Control {
        public abstract void show();
    }

    static class WinButton extends Control {

        @Override
        public void show() {
            System.out.println("I\'m a WinButton: ");
        }
    }

    static class MacButton extends Control {

        @Override
        public void show() {
            System.out.println("I\'m an MacButton: ");
        }
    }

    static class WinLabel extends Control {

        @Override
        public void show() {
            System.out.println("I\'m a WinLabel: ");
        }
    }

    static class MacLabel extends Control {

        @Override
        public void show() {
            System.out.println("I\'m an MacLabel: ");
        }
    }

}

