package class32.designpatterns.creational.builder;

class BuilderDemo {

    public static void main(String[] args) {
        // Create director and builders
        Builder.WaiterDirector waiter = new Builder.WaiterDirector();

        Builder.PizzaBuilder b1 = new Builder.CheesePizzaBuilder();
        Builder.PizzaBuilder b2 = new Builder.MixedPizzaBuilder();

        // Construct two products
        waiter.serve(b1);

        Builder.Pizza p1 = b1.getPizza();
        p1.deliver();

        waiter.serve(b2);
        Builder.Pizza p2 = b2.getPizza();
        p2.deliver();
    }
}
