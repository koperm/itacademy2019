package class32.designpatterns.creational.builder;

import java.util.ArrayList;

class Builder {

    static class WaiterDirector {
        private PizzaBuilder pizzaBuilder;

        //  Builder uses a complex series of steps
        public final void serve(PizzaBuilder pizzaType) {
            this.pizzaBuilder = pizzaType;
            this.pizzaBuilder.buildPizzaBase();
            this.pizzaBuilder.buildToppings();
        }
    }

    static abstract class PizzaBuilder {

        public abstract void buildPizzaBase();

        public abstract void buildToppings();

        public abstract Pizza getPizza();
    }

    static class CheesePizzaBuilder extends PizzaBuilder {

        private Pizza product = new Pizza();

        @Override
        public void buildPizzaBase() {
            this.product.add("Base");
            System.out.println("Adding Base for cheese pizza");
        }

        @Override
        public void buildToppings() {
            this.product.add("Topping : Cheese");
            System.out.println("Adding topping for cheese pizza");
        }

        @Override
        public Pizza getPizza() {
            return this.product;
        }
    }

    static class MixedPizzaBuilder extends PizzaBuilder {

        private Pizza product = new Pizza();

        @Override
        public void buildPizzaBase() {
            this.product.add("Base");
            System.out.println("Adding Base for mixed pizza");
        }

        @Override
        public void buildToppings() {
            System.out.println("Adding topping fpr mixed pizza");
            this.product.add("Topping : Onion");
            System.out.println("Adding more topping for mixed pizza");
            this.product.add("Topping : Tomato");
        }

        @Override
        public Pizza getPizza() {
            return this.product;
        }
    }

    static class Pizza {

        private ArrayList<String> parts = new ArrayList<>();

        public final void add(String part) {
            this.parts.add(part);
        }

        public final void deliver() {
            System.out.println("\nDelivering Pizza with the following");
            for (String part : this.parts) {
                System.out.println(" " + part);
            }

            System.out.println();
        }
    }
}

