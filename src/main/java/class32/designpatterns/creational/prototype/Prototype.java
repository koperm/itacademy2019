package class32.designpatterns.creational.prototype;

class Prototype {

    static abstract class AbstractPrototype implements Cloneable {
        abstract AbstractPrototype create() throws CloneNotSupportedException;
    }

    //  A very heavy object which is time consuming object
    static class ConcretePrototype extends AbstractPrototype {

        @Override
        public AbstractPrototype create() throws CloneNotSupportedException {
            System.out.println("Creating an object from the existing object");
            return (AbstractPrototype) this.clone();
        }
    }

}

