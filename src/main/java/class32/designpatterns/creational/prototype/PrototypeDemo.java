package class32.designpatterns.creational.prototype;

class PrototypeDemo {

    public static void main(String[] args) throws CloneNotSupportedException {
        Prototype.ConcretePrototype p1 = new Prototype.ConcretePrototype();
        System.out.println("Instead of creating the object from scratch we take the prototype");
        Prototype.ConcretePrototype c1 = (Prototype.ConcretePrototype) p1.create();
    }
}
