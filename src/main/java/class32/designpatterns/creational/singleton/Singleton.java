package class32.designpatterns.creational.singleton;

public class Singleton {

    private static Singleton instance = null;

    public static Singleton instance() {
        if (Singleton.instance == null) {
            Singleton.instance = new Singleton();
        }
        return Singleton.instance;
    }

    private Singleton() {
    }
}
