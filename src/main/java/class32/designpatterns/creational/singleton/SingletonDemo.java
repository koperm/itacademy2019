package class32.designpatterns.creational.singleton;

class SingletonDemo {

    public static void main(String[] args) {
        Singleton inst = Singleton.instance();
    }
}
