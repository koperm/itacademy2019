package class5.arrays;

public class ArraysClass {
    public static void main(String[] args) {
        initialization();

        multidimensional();

        integralMaxValue();

        int[] ints4 = new int[]{4, 8, 1, 5};    //18/4 = 4.5
        int[] ints5 = new int[]{-4, -8};
        int[] ints6 = new int[]{};

        System.out.println("Max");
        System.out.println(getMax(ints4));
        System.out.println(getMax(ints5));
        System.out.println(getMax(ints6));

        System.out.println("Avg");
        System.out.println(getAvg(ints4));
        System.out.println(getAvg(ints5));
        System.out.println(getAvg(ints6));

    }

    private static void initialization() {
        int[] ints = new int[10];
        int[] ints2 = new int[]{4, 8, 1, 5};
        int[] ints3 = {1, 2, 3, 4, 5};

        for (int i : ints2) {
            System.out.println(i);
        }

        for (int idx = 0; idx < ints3.length; idx++) {
            System.out.println(idx);
        }
    }

    private static void multidimensional() {
        int[][] ints4 = {{1, 2, 3}, {4, 5}, {6, 7, 8, 9}};
        System.out.println(ints4[1][1]);
        ints4[1][1] = 99;
        System.out.println(ints4[1][1]);
    }

    private static void integralMaxValue() {
        int max = Integer.MAX_VALUE;
        System.out.println("Max: " + max);
        System.out.println(max + 1);
        System.out.println("Min: " + Integer.MIN_VALUE);
    }


    static int getMax(int[] input) {
        if (input.length == 0) {
            System.out.println("Index 0 out of bounds for length 0");
            return Integer.MIN_VALUE;
        }

        int max = input[0];

        for (int i : input) {
            if (max < i) {
                max = i;
            }
        }

        return max;
    }

    static float getAvg(int[] input) {
        if (input.length == 0) {
            return Float.MIN_VALUE;
        }

        float sum = 0;  // try to change type to int and observe the results

        for (int i : input) {
            sum += i;   //sum = sum + i;
        }

        return sum / input.length;
    }

}
