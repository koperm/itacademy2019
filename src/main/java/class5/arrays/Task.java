package class5.arrays;

import java.util.Arrays;

public class Task {

    public static void main(String[] args) {
        sorting2(new int[]{2, 5, 4, 1});
    }

    public static int[] sorting(int[] list) {
        Arrays.sort(list);
        return list;
    }

    public static int[] sorting2(int[] list) {
        for (int j = 0; j < list.length - 1; j++) {
            for (int i = 0; i < list.length - 1; i++) {
                if (list[i] > list[i + 1]) {
                    int temp = list[i + 1];
                    list[i + 1] = list[i];
                    list[i] = temp;
                }
            }
        }
        return list;
    }
}
