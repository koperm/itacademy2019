package class31.regex;

import com.google.common.collect.Lists;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        // TODO Some of examples taken from: https://www.baeldung.com/regular-expressions-java

        String string = "This is first line \n and this is second";
        System.out.println(string);

        System.out.println(Pattern.matches("a*b", "aaaaab"));

        test("foo", "foo");
        test("foo", "foofoo");
        test(".", "foo");

        test("[abc]", "b");
        // TODO Character class is an alternative of characters
        test("[bcr]at", "bat cat rat");
        // TODO Pipe (|) is an alternative of regular expressions
        // https://www.regular-expressions.info/alternation.html
        test("bat|cat|rat", "bat cat rat");
        test("[^abc]", "g");
        test("[^bcr]at", "sat mat eat");
        test("[a-z]", "Two Uppercase alphabets 34 overall");
        test("[a-zA-Z]", "Two Uppercase alphabets 34 overall");
        test("[1-5]", "Two Uppercase alphabets 34 overall");
        test("[0-35]", "Two Uppercase alphabets 34 overall");
        test("[1-3[7-9]]", "123456789");
        test("[1-6&&[3-9]]", "123456789");
        test("[0-9&&[^2468]]", "123456789");

        test("^1", "123");
        test("3$", "123");
        test("\\d", "123");
        test("\\D", "a6c");
        test("\\s", "a c");
        test("\\S", "a c");
        test("\\w", "hi!");
        test("\\W", "hi!");

        test("^dog", "dogs are friendly");
        test("^dog", "are dogs are friendly?");
        test("dog$", "Man's best dog friend is a dog");
        test("dog$", "is a dog man's best friend?");
        test("\\bdog\\b", "a dog is friendly");
        test("\\bdog\\b", "dog is man's best friend");
        test("\\bdog\\b", "snoop dogg is a rapper");

        test("a{3}", "aaaaa");
        test("a{3}", "aa");
        test("a{2,3}", "aaaaa");
        test("a{2,3}?", "aaaaa");
        test("a{2,3}+", "aaaaa");

        // \\a is the alert (bell) character ('\u0007')
        // If you want, you can replace it with any single (non-special) character
        test("a*b", "baaabaaba");
        test("a{0,}b", "baaabaaba");
        test("a+b", "baaabaaba");
        test("a{1,}b", "baaabaaba");
        test("a?b", "baaabaaba");
        test("a{0,1}b", "baaabaaba");

        // GREEDY vs LAZY vs POSSESSIVE
        test("a.*b", "baaabaaba");
        test("a.*?b", "baaabaaba");
        test("a.*+b", "baaabaaba");

        test("a.+b", "baaabaaba");
        test("a.+?b", "baaabaaba");
        test("a.++b", "baaabaaba");

        test("a.?b", "baaabaaba");
        test("a.??b", "baaabaaba");
        test("a.?+b", "baaabaaba");

        test("[Cc][Ii].*\\b", "Cindarella cindarella CIndarella cIndarella");
        test("[Cc][Ii].*?\\b", "Cindarella cindarella CIndarella cIndarella");
        test("[Cc][Ii].*+\\b", "Cindarella cindarella CIndarella cIndarella");

        test("(John)", "John writes about this, and John writes about that, and John writes about everything.");
        test("(John)\\s(w)", "John writes about this, and John writes about that, and John writes about everything.");
        test("(John) (.+?) ", "John writes about this, and John writes about that, and John writes about everything.");

        test("(\\d\\d)", "12");
        test("(\\d\\d)", "1212");
        test("(\\d\\d)\\1", "1212");
        test("(\\d\\d)(\\d\\d)", "1212");
        test("(\\d\\d)\\2", "1212");
        test("(\\d\\d)\\1\\1\\1", "12121212");
        test("(\\d\\d)\\1", "1213");

        System.out.println("dog".matches("dog"));
        System.out.println(Lists.newArrayList("dogs are domestic animals, dogs are friendly".split("dog")));

        replace();
        replaceAll();

        test("\\b[tT]rue\\b", "true True TRUE atrue aTrueB");
        test("\\b[a-zA-Z]{3}\\b", "wet cat four");
        test("\\b[\\d].+?\\b", "555 4re r45 rrr");
        test("\\b[^\\d\\s].+?\\b", "a55 re 5err cat something");
        test("\\b[12]?[0-9]{1,2}\\b", "300 299 301 12 5 02");
        test("\\b[\\w\\.]*?@\\w*?\\..*?\\b", "joe.doe@gmail.com info@outlook.uk @gmail.com a.kowalslki.gmail.com joe@doe joe@doe.");
        test("(\\d+?) \\1", "12 12 123 123");
        test("(a+b)+", "aaababaaaabbbb");  //ababaaaab
        test("((a{1}|a{4})b)+", "aaababaaaabbbb");  //ababaaaab
        test("a+?b", "aaababaaaabbbb");  //ab, aaab, aaaab
    }

    public static void test(String regex, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        int matches = 0;

        System.out.println(new StringBuilder()
                .append("Input string = '").append(input).append("'. ")
                .append("Regex = '").append(regex).append("'. "));

        while (matcher.find()) {

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder
                    .append("Match = '").append(input.substring(matcher.start(), matcher.end())).append("'. ")
                    .append("Match index range = [").append(matcher.start()).append(",").append(matcher.end()).append("). ")
                    .append("Groups = [");

            for (int gr = 0; gr <= matcher.groupCount(); gr++) {
                stringBuilder
                        .append(gr)
                        .append(" : ")
                        .append("[").append(matcher.start(gr)).append("-").append(matcher.end(gr)).append(")")
                        .append(" : ")
                        .append("'").append(matcher.group(gr)).append("'");

                if (gr != matcher.groupCount()) {
                    stringBuilder.append(", ");
                }
            }
            stringBuilder.append("]");

            System.out.println(stringBuilder);
            matches++;
        }

        System.out.println(matches);
    }

    private static void replace() {
        String regex = "dog";
        String input = "dogs are domestic animals, dogs are friendly";
        String replacement = "cat";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        System.out.println(matcher.replaceFirst(replacement));

        System.out.println(input.replaceFirst(regex, replacement));
    }

    private static void replaceAll() {
        String regex = "dog";
        String input = "dogs are domestic animals, dogs are friendly";
        String replacement = "cat";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        System.out.println(matcher.replaceAll(replacement));

        System.out.println(input.replaceAll(regex, replacement));
    }
}
