package class33.datetime;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class LocalTimeMain {
    public static void main(String[] args) {
        // creation
        LocalTime now = LocalTime.now();
        LocalTime sixThirtyParse = LocalTime.parse("06:30");
        LocalTime sixThirtyOf = LocalTime.of(6, 30);

        // arithmetic
        LocalTime sevenThirty = LocalTime.parse("06:30").plus(1, ChronoUnit.HOURS);

        // date parts
        int six = LocalTime.parse("06:30").getHour();

        // relation
        boolean isbefore = LocalTime.parse("06:30").isBefore(LocalTime.parse("07:30"));

        // boundaries
        LocalTime maxTime = LocalTime.MAX;
    }
}
