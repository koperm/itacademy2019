package class33.datetime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class PeriodDurationMain {
    public static void main(String[] args) {

        // Period
        LocalDate initialDate = LocalDate.parse("2007-05-10");
        LocalDate finalDate = initialDate.plus(Period.ofDays(5));

        int five = Period.between(finalDate, initialDate).getDays();
        long five2 = ChronoUnit.DAYS.between(finalDate, initialDate);


        // Duration
        LocalTime initialTime = LocalTime.of(6, 30, 0);
        LocalTime finalTime = initialTime.plus(Duration.ofSeconds(30));

        long thirty = Duration.between(finalTime, initialTime).getSeconds();
        long thirty2 = ChronoUnit.MONTHS.between(finalTime, initialTime);
    }
}
