package class33.datetime;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Set;

public class ZonedDateTimeMain {
    public static void main(String[] args) {
        // zone
        ZoneId zoneId = ZoneId.of("Europe/Paris");
        ZoneId zoneId1 = ZoneId.systemDefault();
        ZoneId zoneId2 = ZoneId.of("UTC+1");
        Set<String> allZoneIds = ZoneId.getAvailableZoneIds();

        // create with ZoneId
        ZonedDateTime zonedDateTime = ZonedDateTime.of(LocalDateTime.now(), zoneId);
        ZonedDateTime zonedDateTime1 = ZonedDateTime.parse("2015-05-03T10:15:30+01:00[Europe/Paris]");
        ZonedDateTime.now();

        // create with OffSetDateTime
        ZoneOffset offset = ZoneOffset.of("+02:00");
        OffsetDateTime offSetByTwo = OffsetDateTime.of(LocalDateTime.now(), offset);

        // arithmetic
        LocalDateTime.now().plusDays(1);
        LocalDateTime.now().minusHours(2);

        // datetime parts
        Month month = LocalDateTime.now().getMonth();

        // relation
        boolean isbefore = LocalDateTime.now().isBefore(LocalDateTime.parse("2015-02-20T06:30:00"));
    }
}
