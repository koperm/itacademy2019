package class33.datetime;

import com.google.common.base.Stopwatch;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

public class MiscMain {
    public static void main(String[] args) {
        // System
        long timestamp = System.currentTimeMillis();

        // Guava
        Stopwatch stopwatch = Stopwatch.createStarted();
        stopwatch.elapsed(TimeUnit.MILLISECONDS);
        stopwatch.elapsed(TimeUnit.MINUTES);

        // Clock
        Clock fixed = Clock.fixed(Instant.EPOCH, ZoneId.systemDefault());
        Clock clock = Clock.systemDefaultZone();
        Clock.tickSeconds(ZoneId.systemDefault());
    }
}
