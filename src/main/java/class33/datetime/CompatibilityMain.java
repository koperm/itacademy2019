package class33.datetime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.GregorianCalendar;

public class CompatibilityMain {
    public static void main(String[] args) {
        LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault());
        LocalDateTime.ofInstant(new GregorianCalendar().toInstant(), ZoneId.systemDefault());
        LocalDateTime.ofEpochSecond(1465817690, 0, ZoneOffset.UTC);
    }
}
