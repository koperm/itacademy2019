package class33.datetime;

import java.time.LocalDateTime;
import java.time.Month;

public class LocalDateTimeMain {
    public static void main(String[] args) {
        // creation
        LocalDateTime.now();
        LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
        LocalDateTime.parse("2015-02-20T06:30:00");

        // arithmetic
        LocalDateTime.now().plusDays(1);
        LocalDateTime.now().minusHours(2);

        // datetime parts
        Month month = LocalDateTime.now().getMonth();

        // relation
        boolean isbefore = LocalDateTime.now().isBefore(LocalDateTime.parse("2015-02-20T06:30:00"));

    }
}
