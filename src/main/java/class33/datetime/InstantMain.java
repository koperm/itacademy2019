package class33.datetime;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class InstantMain {
    public static void main(String[] args) {
        Instant now = Instant.now();
        long epochSecond = now.getEpochSecond();
        int nano = now.getNano();

        Instant later = now.plusSeconds(3);
        Instant later2 = now.plus(3, ChronoUnit.DAYS);
        Instant earlier = now.minusSeconds(3);
    }
}
