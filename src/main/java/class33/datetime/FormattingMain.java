package class33.datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class FormattingMain {
/*
        y   = year   (yy or yyyy)
        M   = month  (MM)
        d   = day in month (dd)
        h   = hour (0-12)  (hh)
        H   = hour (0-23)  (HH)
        m   = minute in hour (mm)
        s   = seconds (ss)
        S   = milliseconds (SSS)
        z   = time zone  text        (e.g. Pacific Standard Time)
        Z   = time zone, time offset (e.g. -0800)
*/

    public static void main(String[] args) throws ParseException {
        // SimpleDateFormat
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        SimpleDateFormat simpleDateFormatWithLocale = new SimpleDateFormat(pattern, new Locale("da", "DK"));
        String date = simpleDateFormat.format(new Date());
        Date date1 = simpleDateFormat.parse("2018-09-09");

        // with TimeZone
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss:SSSZ");
        simpleDateFormat1.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));

        // DateTimeFormatter
/*
        BASIC_ISO_DATE
        ISO_LOCAL_DATE
        ISO_LOCAL_TIME
        ISO_LOCAL_DATE_TIME
        ISO_OFFSET_DATE
        ISO_OFFSET_TIME
        ISO_OFFSET_DATE_TIME
        ISO_ZONED_DATE_TIME
        ISO_INSTANT
        ISO_DATE
        ISO_TIME
        ISO_DATE_TIME
        ISO_ORDINAL_TIME
        ISO_WEEK_DATE
        RFC_1123_DATE_TIME

*/
        String localDateString = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE);
        String formatted = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        String formatted2 = LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.UK));

        DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
        String formattedDate = formatter.format(LocalDate.now());
    }
}
