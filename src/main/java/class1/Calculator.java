package class1;

public class Calculator {
    int startingPoint = 10;

    Calculator() {
        // empty body of a constructor
    }

    void increment(int input2) {
        startingPoint = startingPoint + input2;
    }

    void decrement(int dec) {
        startingPoint = startingPoint - dec;
    }

    int multiplyBy(int multiplier) {
        return startingPoint * multiplier;
    }

    public static void main(String[] args) {
        Calculator calc = new Calculator();
        calc.increment(121121220);
        calc.increment(30);
        calc.decrement(45);
        int result = calc.multiplyBy(5);
    }
}
