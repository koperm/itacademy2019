package class15.maps;

import java.util.HashMap;

public class MapClass {
    public static void main(String[] args) {

        HashMap<String, Person> nameToPerson = new HashMap<>();
        Person mister = new Person("john", 45);
        nameToPerson.put(mister.getName(), mister);

        Person miss = new Person("alice", 45);
        nameToPerson.put(miss.getName(), miss);

        Person amIMisterJohn = nameToPerson.get("john");
        Person amIAlice = nameToPerson.get("alice");

        ////////////////////////////////////////////////////////////

        // poor performance usage of Hashing (see hashCode in Person class)
        HashMap<Person, Person> personToPerson = new HashMap<>();
        Person jh = new Person("john", 45);
        personToPerson.put(jh, jh);

        Person al = new Person("alice", 45);
        personToPerson.put(al, al);

        Person amIJh = personToPerson.get(jh);
        Person amIAl = personToPerson.get(al);


    }
}
