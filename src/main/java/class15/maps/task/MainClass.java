package class15.maps.task;

import com.github.javafaker.Faker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainClass {
    private static final int PERSON_NUMBER = 5;

    public static void main(String[] args) {

        List<Person> employees = new LinkedList<>();

        Person john1 = new Person("John", 45);
        employees.add(john1);

        Person john2 = new Person("John", 45);
        employees.add(john2);

        Person john3 = new Person("Jeffrey", 77);
        employees.add(john3);

        Faker faker = new Faker();
        for (int i = 0; i < PERSON_NUMBER; i++) {
            employees.add(
                    new Person(
                            faker.name().firstName(),
                            faker.number().numberBetween(10, 80)
                    )
            );
        }

        System.out.println("Employees: " + employees);

        Set<Person> uniqueEmployees = new HashSet<>(employees);
        System.out.println("Unique employees: " + uniqueEmployees);

        Map<String, Person> nameToPerson = new HashMap<>();
        for (Person empl : employees) {
            nameToPerson.put(empl.getName(), empl);
        }
        System.out.println("Id to Person: " + nameToPerson);

//        the same as above
//        for (int i = 0; i < employees.size(); i++) {
//            Person empl = employees.get(i);
//            nameToPerson.put(empl.getName(), empl);
//        }

        System.out.println(uniqueEmployees.size());
        System.out.println(nameToPerson.size());
        System.out.println(uniqueEmployees.size() == nameToPerson.size());

    }
}
