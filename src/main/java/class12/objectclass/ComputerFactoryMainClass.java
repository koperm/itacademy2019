package class12.objectclass;

import class12.objectclass.gpu.NVidiaGpu;
import class12.objectclass.gpu.RadeonGpu;

public class ComputerFactoryMainClass {
    public static void main(String[] args) {

        System.out.println("NVidia different instances (overridden hashCode and equals)");
        NVidiaGpu nVidiaGpu = new NVidiaGpu();
        NVidiaGpu nVidiaGpu2 = new NVidiaGpu();

        System.out.println("hashcodes: " + nVidiaGpu.hashCode() + " vs " + nVidiaGpu2.hashCode());
        System.out.println("references: " + (nVidiaGpu == nVidiaGpu2));
        System.out.println("equals: " + nVidiaGpu.equals(nVidiaGpu2));

        System.out.println("\nRadeon different instances (basic hashCode and equals)");
        RadeonGpu radeonGpu = new RadeonGpu();
        RadeonGpu radeonGpu2 = new RadeonGpu();

        System.out.println("hashcodes: " + radeonGpu.hashCode() + " vs " + radeonGpu2.hashCode());
        System.out.println("references: " + (radeonGpu == radeonGpu2));
        System.out.println("equals: " + radeonGpu.equals(radeonGpu2));


        System.out.println("\nNVidia the same instance (overridden hashCode and equals)");
        NVidiaGpu nVidiaGpu3 = new NVidiaGpu();
        NVidiaGpu nVidiaGpu4 = nVidiaGpu3;

        System.out.println("hashcodes: " + nVidiaGpu3.hashCode() + " vs " + nVidiaGpu4.hashCode());
        System.out.println("references: " + (nVidiaGpu3 == nVidiaGpu4));
        System.out.println("equals: " + nVidiaGpu3.equals(nVidiaGpu4));

        System.out.println("\nRadeon the same instance (basic hashCode and equals)");
        RadeonGpu radeonGpu3 = new RadeonGpu();
        RadeonGpu radeonGpu4 = radeonGpu3;

        System.out.println("hashcodes: " + radeonGpu3.hashCode() + " vs " + radeonGpu4.hashCode());
        System.out.println("references: " + (radeonGpu3 == radeonGpu4));
        System.out.println("equals: " + radeonGpu3.equals(radeonGpu4));
    }
}
