package class12.objectclass.gpu;

import java.util.concurrent.ThreadLocalRandom;

public class NVidiaGpu extends Gpu {
    @Override
    int getG3dMark() {
        return 5000;
    }

    @Override
    public String toString() {
        return "NVidiaGpu{" +
                "g3dMark=" + getG3dMark() +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return ThreadLocalRandom.current().nextInt(0, 10);
    }
}
