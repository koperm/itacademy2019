package class17.generics.classes;

public interface Pair <K, V> {
    K getKey();

    V getValue();
}