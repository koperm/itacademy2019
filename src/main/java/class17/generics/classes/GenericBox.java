package class17.generics.classes;

public class GenericBox<T> {
    private T t;

    public void add(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    public static void main(String[] args) {
        GenericBox<Integer> integerGenericBox = new GenericBox<Integer>();
        GenericBox<Integer> integerGenericBox1 = new GenericBox<>();    // the same as above
        GenericBox<String> stringGenericBox = new GenericBox<String>();

        integerGenericBox.add(10);
        integerGenericBox.add(new Integer(10));
//        integerGenericBox.add("fadsf");   // TODO why?
        stringGenericBox.add("Hello World");

        Integer integer = integerGenericBox.get();
//        String integer2 = integerGenericBox.get();    // TODO why?
        System.out.printf("Integer Value :%d\n\n", integerGenericBox.get());
        System.out.printf("String Value :%s\n", stringGenericBox.get());
    }
}
