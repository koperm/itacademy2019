package class17.generics.pecs;

import java.util.List;

public class Main {
    private void test() {
        List<Object> objectList = List.of();
        List<Plant> plantList = List.of();
        List<Fruit> fruitList = List.of();
        List<Banana> bananaList = List.of();
        List<RottenBanana> rottenBananaList = List.of();
        List<Apple> appleList = List.of();

////////////////////////////////////////////////////////////////////////////////////////////////////

        /* PRODUCER / get something and assign it to type */

//        List<? extends Fruit> producerOrSrc6 = objectList;        // TODO Object is not a Fruit
//        List<? extends Fruit> producerOrSrc1 = plantList;         // TODO Plant is not a Fruit
        List<? extends Fruit> producerOrSrc2 = fruitList;           // TODO Fruit is a Fruit
        List<? extends Fruit> producerOrSrc3 = bananaList;          // TODO Banana is a Fruit
        List<? extends Fruit> producerOrSrc4 = rottenBananaList;    // TODO RottenBanana is a Fruit
        List<? extends Fruit> producerOrSrc5 = appleList;           // TODO Apple is a Fruit

        List<? extends Fruit> producerOrSrc = List.of();
//        producerOrSrc.add(new Object());          // TODO Object does not extend Fruit
//        producerOrSrc.add(new Plant());           // TODO Plant does not extend Fruit
//        producerOrSrc.add(new Fruit());           // TODO producerOrSrc may be a new ArrayList<RottenBanana>() but you try to put Fruit (Fruit is not a RottenBanana)
//        producerOrSrc.add(new Banana());          // TODO producerOrSrc may be a new ArrayList<Apple>() but you try to put Banana
//        producerOrSrc.add(new RottenBanana());    // TODO producerOrSrc may be a new ArrayList<Apple>() but you try to put RottenBanana
//        producerOrSrc.add(new Apple());           // TODO producerOrSrc may be ArrayList<RottenBanana>() but and you try to put Apple
        producerOrSrc.add(null);                    // TODO always allowed

        Object object = producerOrSrc.get(0);               // TODO Fruit or Fruit's child is an Object
        Plant plant = producerOrSrc.get(0);                 // TODO Fruit or Fruit's child is a Plant
        Fruit fruit = producerOrSrc.get(0);                 // TODO Fruit or Fruit's child is a Fruit
//        Banana banana = producerOrSrc.get(0);             // TODO Fruit or Fruit's child does not have to be a Banana. May be for example an Apple
//        RottenBanana rottenBanana = producerOrSrc.get(0); // TODO Fruit or Fruit's child does not have to be a RottenBanana. May be for example an Apple
//        Apple apple = producerOrSrc.get(0);               // TODO Fruit or Fruit's child does not have to be a Apple. May be for example an RottenBanana

////////////////////////////////////////////////////////////////////////////////////////////////////

        /* CONSUMER / put something into it */

        List<? super Banana> consumerOrDst0 = objectList;           // TODO Object is a parent of Banana
        List<? super Banana> consumerOrDst1 = plantList;            // TODO Plant is a parent of Banana
        List<? super Banana> consumerOrDst2 = fruitList;            // TODO Fruit is a parent of Banana
        List<? super Banana> consumerOrDst3 = bananaList;           // TODO Banana is a Banana
//        List<? super Banana> consumerOrDst4 = rottenBananaList;   // TODO RottenBanana is NOT a parent of Banana
//        List<? super Banana> consumerOrDst5 = appleList;          // TODO Apple is NOT a parent of Banana

        List<? super Banana> consumerOrDst = List.of();
//        consumerOrDst.add(new Object());      // TODO producerOrSrc may be a new ArrayList<Plant>() and you try to put Object
//        consumerOrDst.add(new Plant());       // TODO producerOrSrc may be a new ArrayList<Fruit>() and you try to put Plant
//        consumerOrDst.add(new Fruit());       // TODO producerOrSrc may be a new ArrayList<Banana>() and you try to put Fruit
        consumerOrDst.add(new Banana());        // TODO producerOrSrc holds entities which type is parent of Banana so I can put any Banana and its children
        consumerOrDst.add(new RottenBanana());  // TODO producerOrSrc holds entities which type is parent of Banana so I can put any Banana and its children
//        consumerOrDst.add(new Apple());       // TODO producerOrSrc holds entities which type is parent of Banana so I can put only Banana and any child of Banana
        consumerOrDst.add(null);                // TODO simply allowed

        Object object1 = consumerOrDst.get(0);                  // TODO every type is a child of Object
//        Plant plant11 = consumerOrDst.get(0);                 // TODO producerOrSrc may be a new ArrayList<Object>();
//        Fruit frui1t = consumerOrDst.get(0);                  // TODO producerOrSrc may be a new ArrayList<Object>();
//        Banana banana1 = consumerOrDst.get(0);                // TODO producerOrSrc may be a new ArrayList<Object>();
//        RottenBanana rottenBanana1 = consumerOrDst.get(0);    // TODO producerOrSrc may be a new ArrayList<Object>();
//        Apple apple1 = consumerOrDst.get(0);                  // TODO producerOrSrc may be a new ArrayList<Object>();
    }

    private static class Plant {}

    private static class Fruit extends Plant {}

    private static class Banana extends Fruit {}

    private static class RottenBanana extends Banana {}

    private static class Apple extends Fruit {}
}
