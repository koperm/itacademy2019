package class17.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsMain {

    public static void main(String[] args) {

        // Raw type
        List list = new ArrayList();
        list.add("one");
        list.add(123);

        String strVal = (String) list.get(0);
        Integer intVal = (Integer) list.get(1);

        Object entity1 = list.get(0);
        Object entity2 = list.get(1);

        // Generic type
        List<String> list2 = new ArrayList<>();
        list2.add("one");
//        list2.add(123);  // TODO why?

        String strVal1 = list2.get(0);
//        Integer intVal1 = list2.get(1);   // TODO why?

    }


}
