package class17.generics.methods;

import class15.maps.Person;

import java.util.ArrayList;
import java.util.List;

public class GenericMethod {

    // generic type T
    static <T> List<T> fromArrayToList(T[] array) {
        List<T> list = new ArrayList<>();
        for (T val : array) {
            list.add(val);
        }
        return list;
    }

    // bounded generic type
    static <T extends Number> List<T> fromArrayToList1(T[] array) {
        return fromArrayToList(array);
    }

    // another bounded generic type
    static <T extends Number & Comparable> List<T> fromArrayToList2(T[] array) {
        return fromArrayToList(array);
    }

    // Info: there are also wildcard types, unbounded generic types e.g. List<?>
    public static void main(String[] args) {
        List<Integer> integers = fromArrayToList(new Integer[]{1, 2, 3});
        fromArrayToList(new Person[]{new Person("jan", 34)});
        List<String> strings = fromArrayToList(new String[]{"one", "two", "three"});
        List<String> strings2 = fromArrayToList(new String[]{"one", "two", "three"});

    }
}
