package class17.generics.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class GenericComparator {

    public static void main(String[] args) {
        Set<Person> people = Set.of(
                new Person("Anna", 25),
                new Person("Adam", 20),
                new Person("John", 22));

        List<Person> sortedPeople = sort(people, new PersonAgeComparator());
        System.out.println(sortedPeople);

        List<Integer> ints = List.of(30, 20, 40);
        List<Integer> reversedInts = sort(ints, Comparator.reverseOrder());
        System.out.println(reversedInts);
    }

    static <T> List<T> sort(Collection<T> input, Comparator<T> comparator) {
        List<T> arrayList = new ArrayList<>(input);
        arrayList.sort(comparator);
//        Collections.sort(arrayList, comparator);
        return arrayList;
    }


}
