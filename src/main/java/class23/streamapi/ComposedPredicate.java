package class23.streamapi;

import java.util.function.Predicate;

public class ComposedPredicate {
    public static void main(String[] args) {
        Predicate<String> startsWithA = text -> text.startsWith("A");
        Predicate<String> endsWithX = text -> text.endsWith("Z");

        // Intersection (AND)
        Predicate<String> composed1 = text -> startsWithA.test(text) && endsWithX.test(text);
        Predicate<String> composed2 = startsWithA.and(endsWithX);

        // Alternative (OR)
        Predicate<String> composed3 = startsWithA.or(endsWithX);

        String input = "A Functional language is a buzZ";
        boolean result1 = composed1.test(input);
        boolean result2 = composed2.test(input);
    }
}
