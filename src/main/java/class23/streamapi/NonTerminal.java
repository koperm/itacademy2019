package class23.streamapi;

import com.google.common.base.Splitter;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class NonTerminal {
    public static void main(String[] args) {
        // filter & map
        Stream<String> stringStream = List.of("one", "two", "three").stream()
                .filter(s -> s.startsWith("t"))
                .map(s -> s.toUpperCase());

        // flat
        List<List<String>> listOfList = List.of(split("one"), split("two"), split("three"));
        List<String> flattenList = listOfList.stream()
                .flatMap(List::stream)
                .collect(toList());

        // distinct
        List<String> distinct = List.of("one", "two", "three", "two").stream()
                .distinct()
                .collect(toList());

        // limit
        List<String> limit = List.of("one", "two", "three", "two").stream()
                .limit(2)
                .collect(toList());

        // skip
        List<String> skip = List.of("one", "two", "three", "two").stream()
                .skip(2)
                .collect(toList());

        // peek
        List<String> peek = List.of("one", "two", "three", "two").stream()
                .peek(System.out::println)
                .collect(toList());

        // sorted
        List<String> sorted = List.of("one", "two", "three", "two").stream()
                .sorted(Comparator.naturalOrder())
                .collect(toList());

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // mix
        List.of("oNe", "tWo", "thRee")
                .stream()
                .map(String::toLowerCase)
                .map(str -> str.substring(1, str.length()))
                .forEach(System.out::println);  // terminal operation

        /*
        Given tthe List.of("one", "two", "three", "four"):
        Filter all words that consist of at least one "e" letter
        Map every word to camel-case version e.g "three" -> "tHrEe"
        Collect every stream to list
         */

        List.of("one", "two", "three", "four")
                .stream()
                .filter((arg) -> arg.contains("e"))
//                .map(s -> CaseUtils.toCamelCase(s, false))
                .collect(Collectors.toList());

    }

    private static List<String> split(String input) {
        return Splitter.fixedLength(1).splitToList(input);
    }
}
