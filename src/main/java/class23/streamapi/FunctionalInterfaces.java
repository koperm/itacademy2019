package class23.streamapi;

import java.util.Comparator;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class FunctionalInterfaces {
    public static void main(String[] args) {
        // Function
        Function<Long, String> function1 = inputLong -> String.valueOf(inputLong);
        Function<Long, String> function2 = String::valueOf;
        Function<Long, String> function3 = new Converter();

        String outputString1 = function1.apply(123L);
        String outputString2 = function2.apply(123L);
        String outputString3 = function3.apply(123L);

        ////////////////////////////////////////////////////////////////////////////////////////////////

        // Consumer
        Consumer<Car> consumer1 = new Consumer<>() {
            @Override
            public void accept(Car car) {
                car.drive();
            }
        };
        Consumer<Car> consumer2 = car -> car.drive();
        Consumer<Car> consumer3 = Car::drive;

        consumer1.accept(new Car());
        consumer2.accept(new Car());
        consumer3.accept(new Car());

        ////////////////////////////////////////////////////////////////////////////////////////////////

        // Supplier
        Supplier<Car> ferrariSupplier = new Supplier<>() {
            @Override
            public Car get() {
                return new Car();
            }
        };
        Supplier<Car> truckSupplier = () -> new Car();
        Supplier<Car> carSupplier = Car::new;

        Car ferrari = ferrariSupplier.get();
        Car truck = truckSupplier.get();
        Car car = carSupplier.get();

        ////////////////////////////////////////////////////////////////////////////////////////////////

        // Predicate
        Predicate<String> predicate1 = input -> input.equals("Some text");
        Predicate<String> predicate3 = new NullChecker<>();

        boolean out1 = predicate1.test("different text");
        boolean out2 = predicate3.test("different text");

        ////////////////////////////////////////////////////////////////////////////////////////////////

        // UnaryOperator
        UnaryOperator<String> unaryOperator = inputString -> inputString.concat(" postfixString");
        String out = unaryOperator.apply("prefixString");

        // BinaryOperator
        BinaryOperator<String> binaryOperator = (input1, input2) -> input1.concat(input2);

        ////////////////////////////////////////////////////////////////////////////////////////////////

        // Comparator
        Comparator<String> comparator1 = (input1, input2) -> input1.length() - input2.length();
        Comparator<String> comparator2 = (input1, input2) -> input1.compareTo(input2);
        Comparator<String> comparator3 = comparator2.reversed();

        int compare1 = comparator1.compare("one", "two");
        int compare2 = comparator2.compare("one", "two");
        int compare3 = comparator3.compare("one", "two");
    }

    private static class Converter implements Function<Long, String> {
        @Override
        public String apply(Long inputLong) {
            return "" + inputLong;
        }
    }

    private static class Car {
        void drive() {
            System.out.println("I am driving my Fiat Panda");
        }
    }

    private static class NullChecker<T> implements Predicate<T> {
        @Override
        public boolean test(T o) {
            return o != null;
        }
    }
}
