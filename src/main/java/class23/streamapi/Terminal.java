package class23.streamapi;

import com.google.common.base.Splitter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class Terminal {
    public static void main(String[] args) {
        // anyMatch //TODO short-circuit
        boolean match = List.of("one", "two", "three").stream()
                .anyMatch(s -> s.startsWith("t"));

        // allMatch
        boolean match1 = List.of("one", "two", "three").stream()
                .allMatch(s -> s.length() < 10);

        // noneMatch
        boolean match2 = List.of("one", "two", "three").stream()
                .noneMatch(s -> s.endsWith("x"));

        // collect
        List<String> list = List.of("one", "two", "three").stream()
                .collect(Collectors.toList());

        Map<String, Integer> strToLenght = List.of("one", "two", "three").stream()
                .limit(2)
                .collect(toMap((str) -> str, (str) -> str.length()));

        // count
        long count = List.of("one", "two", "three").stream().count();

        // findAny
        List.of("one", "two", "three").stream()
                .findAny()
                .ifPresent(System.out::println);

        // findFirst
        List.of("one", "two", "three").stream()
                .findFirst()
                .ifPresent(System.out::println);

        // forEach
        List.of("one", "two", "three").stream()
                .forEach(System.out::println);

        // min
        List.of("one", "two", "three").stream()
                .min(String::compareTo)
                .ifPresent(System.out::println);

        // max
        List.of("one", "two", "three").stream()
                .max(String::compareTo)
                .ifPresent(System.out::println);

        // reduce
        List.of("one", "two", "three").stream()
                .reduce((value, agregator) -> agregator.concat("+").concat(value))
                .ifPresent(System.out::println);

        // toArray
        Object[] objects = List.of("one", "two", "three").stream()
                .toArray();
    }

    private static List<String> split(String input) {
        return Splitter.fixedLength(1).splitToList(input);
    }
}
