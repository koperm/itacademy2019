package class23.streamapi;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CreateStream {
    public static void main(String[] args) {

        Stream<String> concatStream = Stream.concat(List.of("one", "two").stream(), List.of("three", "four").stream());
        Stream<String> streamOf = Stream.of("one", "two", "three");
        Stream<String> arrayStream = Stream.of(new String[]{"one", "two", "three"});
        Stream<String> listStream = List.of("one", "two", "three").stream();
        Stream<String> setStream = Set.of("one", "two", "three").stream();
        Map.of().entrySet().stream();
        Stream<String> streamGenerate = Stream.generate(() -> RandomStringUtils.randomAlphabetic(5));

        List<Integer> ints = Stream.of(1, 2, 3, 4, 5)
                .peek(System.out::println)
                .collect(Collectors.toList());

        List<Integer> ints2 = IntStream.of(1, 2, 3, 4, 5)
                .boxed()    // TODO why do I have to invoke boxed()?
                .collect(Collectors.toList());

        IntStream range = IntStream.range(1, 10);

        int sum = List.of(33, 45)
                .stream()
                .mapToInt(i -> i)
                .sum();

    }
}
