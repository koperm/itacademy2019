package class28.annotationssolid.annotations.processing;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

public class ProcessAnnotationExample {
    public static void main(String[] args) throws NoSuchMethodException {

        Class<DemoClassJavaFileInfo> demoClassObj = DemoClassJavaFileInfo.class;

        readAnnotationOn(demoClassObj);

        Method method = demoClassObj.getMethod("getString");

        readAnnotationOn(method);
    }

    static void readAnnotationOn(AnnotatedElement element) {
        System.out.println("\n Finding annotations on " + element.getClass().getName());
        Annotation[] annotations = element.getAnnotations();

        for (Annotation annotation : annotations) {
            if (annotation instanceof JavaFileInfo) {
                JavaFileInfo fileInfo = (JavaFileInfo) annotation;
                System.out.println("Author :" + fileInfo.author());
                System.out.println("Version :" + fileInfo.version());
            }
        }
    }
}