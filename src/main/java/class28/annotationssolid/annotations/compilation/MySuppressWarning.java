package class28.annotationssolid.annotations.compilation;

import java.util.ArrayList;
import java.util.List;

public class MySuppressWarning {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("one");
        list.add(123);
    }
}
