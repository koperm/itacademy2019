package class28.annotationssolid.annotations.compilation;


/**
 * @deprecated Use MyNotDeprecated instead.
 */
@Deprecated
public class MyDeprecated {

    @Deprecated
    protected String someField;

    @Deprecated
    public String getter() {
        return "";
    }

    public static void main(String[] args) {
        MyDeprecated myDeprecated = new MyDeprecated();
        String someField = myDeprecated.someField;
        myDeprecated.getter();
    }
}