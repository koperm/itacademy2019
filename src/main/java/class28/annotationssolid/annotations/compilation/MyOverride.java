package class28.annotationssolid.annotations.compilation;

public class MyOverride {

    private static class MySuperClass {
        public void doTheThing() {
            System.out.println("Do the thing");
        }
    }

    private static class MySubClass extends MySuperClass {
        @Override
        public void doTheThing() {
            System.out.println("Do it differently");
        }
    }

    public static void main(String[] args) {
        MySuperClass mySubClass = new MySubClass();
    }
}
