package class28.annotationssolid.annotations.compilation;

public class DeprecatedMain {
    public static void main(String[] args) {
        MyDeprecated myDeprecated = new MyDeprecated();
        String someField = myDeprecated.someField;
        myDeprecated.getter();
    }
}
