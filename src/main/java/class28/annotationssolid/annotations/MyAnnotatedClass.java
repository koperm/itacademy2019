package class28.annotationssolid.annotations;

public class MyAnnotatedClass {

    @MyAnnotation0("some value")
    private static class MyAnnotatedClass0 {
    }

    @MyAnnotation01
    private static class MyAnnotatedClass01 {
    }

    @MyAnnotation1(
            name = "first name",
            age = 37,
            value = "123",
            newNames = {"alias1", "alias2"}
    )
    private static class MyAnnotatedClass1 {
    }

    @MyAnnotation2(
            name = "first name",
            age = 37,
            newNames = {"alias1", "alias2"}
    )
    private static class MyAnnotatedClass2 {
    }

    @MyAnnotation3()
    private static class MyAnnotatedClass3 {
    }

    //    @MyAnnotation4()
    private static class MyAnnotatedClass4 {
        @MyAnnotation4()
        private void method() {
        }
    }

    private static class MyAnnotatedSubClass5 extends MyAnnotatedSuperClass5 {
    }

    @MyAnnotation5()
    private static class MyAnnotatedSuperClass5 {
    }

    @MyAnnotation6()
    private static class MyAnnotatedClass6 {
    }

    @MyAnnotation7({IndexOutOfBoundsException.class, NullPointerException.class})
    public static void throwingExc1() {
        // code throws any Ecx
    }

    @MyAnnotation8(IndexOutOfBoundsException.class)
    @MyAnnotation8(NullPointerException.class)
    public static void throwingExc2() {
        //...
    }
}
