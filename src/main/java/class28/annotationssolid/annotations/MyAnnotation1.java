package class28.annotationssolid.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@interface MyAnnotation0 {
    String value();
}


@interface MyAnnotation01 {
    String value() default "regsdfgdf";
}

@interface MyAnnotation1 {

    String value();

    String name();

    int age();

    String[] newNames();
}

@interface MyAnnotation2 {

    String value() default "default value";

    String name();

    int age();

    String[] newNames();
}

@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation3 {

    String value() default "default value";
}

@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
@interface MyAnnotation4 {

    String value() default "default value";
}

@Inherited
@interface MyAnnotation5 {

    String value() default "default value";
}

@Documented
@interface MyAnnotation6 {

    String value() default "default value";
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface MyAnnotation7 {
    Class<? extends Exception>[] value();
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface MyAnnotation8Container {
    MyAnnotation8[] value();
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(MyAnnotation8Container.class)
@interface MyAnnotation8 {
    Class<? extends Exception> value();
}