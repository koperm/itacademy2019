package class28.annotationssolid.solid;

class Document {
    //
}

// wrong one
interface Machine {
    void print(Document d);

    void fax(Document d) throws Exception;

    void scan(Document d) throws Exception;
}

class MultiFunctionPrinter implements Machine {
    public void print(Document d) {
        //
    }

    public void fax(Document d) {
        //
    }

    public void scan(Document d) {
        //
    }
}

class OldFashionedPrinter implements Machine {
    public void print(Document d) {
        // yep
    }

    // break the principle, because do not need fax() method and throws Exception
    public void fax(Document d) throws Exception {
        throw new Exception();
    }

    // break the principle, because do not need fax() method and throws Exception
    public void scan(Document d) throws Exception {
        throw new Exception();
    }
}

// correct one
interface Printer {
    void print(Document d) throws Exception;
}

interface IScanner {
    void scan(Document d) throws Exception;
}

interface MultiFunctionDevice extends Printer, IScanner {

}

class JustAPrinter implements Printer {
    public void print(Document d) {

    }
}

class Photocopier implements Printer, IScanner {
    public void print(Document d) {
        //
    }

    public void scan(Document d) {
        //
    }
}

class MultiFunctionMachine implements MultiFunctionDevice {
    // compose this out of several modules
    private Printer printer;
    private IScanner scanner;

    public MultiFunctionMachine(Printer printer, IScanner scanner) {
        this.printer = printer;
        this.scanner = scanner;
    }

    public void print(Document d) throws Exception {
        printer.print(d);
    }

    public void scan(Document d) throws Exception {
        scanner.scan(d);
    }
}