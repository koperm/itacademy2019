package class28.annotationssolid.solid;

import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class Journal {
    private static int count = 0;
    private final List<String> entries = new ArrayList<>();

    public void addEntry(String text) {
        entries.add("" + (++count) + ": " + text);
    }

    public void removeEntry(int index) {
        entries.remove(index);
    }

    @Override
    public String toString() {
        return String.join(System.lineSeparator(), entries);
    }

    // here we break SRP
    public void save(String filename) throws Exception {
        try (PrintStream out = new PrintStream(filename)) {
            out.println(toString());
        }
    }

    public void load(String filename) {
        // here we break SRP
    }


    public void load(URL url) {
        // here we break SRP
    }
}

// handles the responsibility of persisting objects
class Persistence {

    public void saveToFile(Journal journal, String filename, boolean overwrite) throws Exception {
        if (overwrite || new File(filename).exists()) {
            try (PrintStream out = new PrintStream(filename)) {
                out.println(journal.toString());
            }
        }
    }

    public void load(Journal journal, String filename) {
        // some impl
    }

    public void load(Journal journal, URL url) {
        // some impl
    }
}

class SRPDemo {
    public static void main(String[] args) throws Exception {
        Journal j = new Journal();
        j.addEntry("I cried today");
        j.addEntry("I ate a bug");
        System.out.println(j);

        Persistence p = new Persistence();
        Path resourcesPath = Paths.get("src", "main", "resources");
        Path filePath = resourcesPath.resolve("journal.txt").toAbsolutePath();
        p.saveToFile(j, filePath.toString(), true);

        // windows!
        Runtime.getRuntime().exec("D:\\Programs\\VSCode\\Microsoft VS Code\\Code.exe " + filePath);
    }
}