package class11.abstracts;

public abstract class Construction {

    abstract protected void buildFundamentals();

    abstract protected void buildWalls();

    protected void buildRoof() {
        System.out.println("Construction buildRoof");
    }

    void build() {
        buildFundamentals();
        buildWalls();
        buildRoof();
    }

}
