package class11.abstracts;

public class GardenHouse extends Construction {

    @Override
    protected void buildFundamentals() {
        System.out.println("Garden House fundamentals");
    }

    @Override
    protected void buildWalls() {
        System.out.println("Garden House walls");
    }

    @Override
    protected void buildRoof() {
        System.out.println("Garden House roof");
    }

    void move() {
        System.out.println("Garden house move");
    }
}
