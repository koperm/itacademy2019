package class11.abstracts;

public abstract class WellConstruction extends Construction {

    @Override
    protected void buildFundamentals() {
        System.out.println("Dig until you find water");
    }

    abstract void settleRings();
}
