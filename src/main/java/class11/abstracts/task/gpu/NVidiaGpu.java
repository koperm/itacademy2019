package class11.abstracts.task.gpu;

public class NVidiaGpu extends Gpu {
    @Override
    int getG3dMark() {
        return 5000;
    }

    @Override
    public String toString() {
        return "NVidiaGpu{" +
                "g3dMark=" + getG3dMark() +
                '}';
    }
}
