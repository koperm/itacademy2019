package class11.abstracts.task.gpu;

public abstract class Gpu {
    abstract int getG3dMark();

    @Override
    public String toString() {
        return "Gpu{" +
                "g3dMark=" + getG3dMark() +
                '}';
    }
}
