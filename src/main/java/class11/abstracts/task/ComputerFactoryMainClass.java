package class11.abstracts.task;

import class11.abstracts.task.gpu.NVidiaGpu;
import class11.abstracts.task.gpu.RadeonGpu;
import class11.abstracts.task.processor.AmdProcessor;
import class11.abstracts.task.processor.IntelProcessor;

public class ComputerFactoryMainClass {
    public static void main(String[] args) {
        AmdProcessor amdProcessor = new AmdProcessor();
        IntelProcessor intelProcesso = new IntelProcessor();

        NVidiaGpu nVidiaGpu = new NVidiaGpu();
        RadeonGpu radeonGpu = new RadeonGpu();

        ComputerFactory computerFactory = new ComputerFactory();
        computerFactory.installProcessor(amdProcessor);
        computerFactory.installGpu(nVidiaGpu);

        System.out.println(computerFactory);

        computerFactory.installProcessor(intelProcesso);
        computerFactory.installGpu(radeonGpu);

        System.out.println(computerFactory);

    }
}
