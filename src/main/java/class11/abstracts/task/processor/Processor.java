package class11.abstracts.task.processor;

public abstract class Processor {

    abstract int getMHz();

    @Override
    public String toString() {
        return "Processor{" +
                "mHz=" + getMHz() +
                '}';
    }
}
