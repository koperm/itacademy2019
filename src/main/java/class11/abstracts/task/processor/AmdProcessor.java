package class11.abstracts.task.processor;

public class AmdProcessor extends Processor {

    @Override
    int getMHz() {
        return 200;
    }

    @Override
    public String toString() {
        return "AmdProcessor{" +
                "mHz=" + getMHz() +
                '}';
    }
}
