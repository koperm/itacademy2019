package class11.abstracts.task.processor;

public class IntelProcessor extends Processor {

    @Override
    int getMHz() {
        return 100;
    }
}
