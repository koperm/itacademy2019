package class11.abstracts.task;

import class11.abstracts.task.gpu.Gpu;
import class11.abstracts.task.processor.Processor;

public class ComputerFactory {
    private Processor processor;
    private Gpu gpu;

    void installProcessor(Processor processor) {
        this.processor = processor;
    }

    Processor getProcessor() {
        return processor;
    }

    void installGpu(Gpu gpu) {
        this.gpu = gpu;
    }

    Gpu getGpu() {
        return gpu;
    }

    @Override
    public String toString() {
        return "ComputerFactory{" +
                "processor=" + processor +
                ", gpu=" + gpu +
                '}';
    }
}
