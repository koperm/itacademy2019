package class11.abstracts;

public class MainClass {
    public static void main(String[] args) {
//        Construction construction = new Construction();   // TODO why?

        System.out.println("1");

        GardenHouse gardenHouse = new GardenHouse();
        gardenHouse.buildFundamentals();
        gardenHouse.buildWalls();
        gardenHouse.buildRoof();
        gardenHouse.move();
        gardenHouse.build();

        System.out.println("2");

        Construction construction = new GardenHouse();
        construction.buildFundamentals();
        construction.buildWalls();
        construction.buildRoof();
//        construction.move();  // TODO why?
        construction.build();

        System.out.println("3");

        Construction constructionTwo = construction;
        constructionTwo.buildFundamentals();
        constructionTwo.buildWalls();
        constructionTwo.buildRoof();
//        constructionTwo.move();  // TODO why?
        constructionTwo.build();

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        System.out.println("4");

        GardenHouse gardenCastle = new GardenCastle();
        gardenCastle.buildFundamentals();
        gardenCastle.buildWalls();
        gardenCastle.buildRoof();
        gardenCastle.move();
        gardenCastle.build();

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        System.out.println("5");

        BlockOfFlats block = new BlockOfFlats();
        block.buildFundamentals();
        block.buildWalls();
        block.buildRoof();
        block.build();

//        GardenHouse constructionThree = block;  // TODO why?

        System.out.println("6");

        Construction constructionThree = block;
        constructionThree.buildFundamentals();
        construction.buildWalls();
        constructionThree.buildRoof();
        constructionThree.build();

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        System.out.println("7");

        WellConstruction well = new WellImplementation();
        well.buildFundamentals();
        well.buildWalls();
        well.buildRoof();
        well.settleRings();
        well.build();

        System.out.println("8");

        Construction constructionFour = well;
        constructionFour.buildFundamentals();
        constructionFour.buildWalls();
        constructionFour.buildRoof();
//        constructionFour.settleRings();    // TODO why?
        constructionFour.build();

    }
}
