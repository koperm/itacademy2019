package class11.abstracts;

public class GardenCastle extends GardenHouse {
    @Override
    protected void buildFundamentals() {
        System.out.println("GardenCastle build brick foundations");
    }

    @Override
    protected void buildWalls() {
        System.out.println("GardenCastle walls");
    }

    @Override
    protected void buildRoof() {
        System.out.println("Not enough money to build a beautiful roof. Need to be used from GardenHouse");
        super.buildWalls();
    }
}
