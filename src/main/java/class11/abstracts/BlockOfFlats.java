package class11.abstracts;

public class BlockOfFlats extends Construction {
    @Override
    protected void buildFundamentals() {
        System.out.println("BlockOfFlats fundamentals");
    }

    @Override
    protected void buildWalls() {
        System.out.println("BlockOfFlats Walls");
    }
}
