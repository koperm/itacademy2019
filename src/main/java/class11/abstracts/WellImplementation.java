package class11.abstracts;

public class WellImplementation extends WellConstruction {
    @Override
    void settleRings() {    // why not protected?
        System.out.println("Roll it babe");
    }

    @Override
    protected void buildWalls() {
        settleRings();
    }
}
