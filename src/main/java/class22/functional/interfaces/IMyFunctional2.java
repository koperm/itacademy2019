package class22.functional.interfaces;

@FunctionalInterface
public interface IMyFunctional2 {
    int execute(int a, int b);

    // Possible lambdas that match the interface

//    (aa, bb) -> {
//        return aa - bb;
//    }

//    (aa, bb) -> {
//        return aa + bb;
//    }
}
