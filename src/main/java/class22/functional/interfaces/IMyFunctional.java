package class22.functional.interfaces;

@FunctionalInterface
public interface IMyFunctional {
    void execute();

    default void specialDefaultExecute(){
        System.out.println("specialDefaultExecute");
    }

    static void specialStaticExecute(){
        System.out.println("specialStaticExecute");
    }
}

