package class22.functional.lambda;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class JavaFunctionalInterfacesMain {
    public static void main(String[] args) {
        Supplier<String> exp1 = () -> "No param lambda expression";
        Supplier<Integer> exp11 = () -> "No param lambda expression".length();
        exp1.get();

        Consumer<String> exp2 = (prefix) -> System.out.println(prefix + "One param lambda expression");
        Consumer<String> exp3 = prefix -> System.out.println(prefix + "One param lambda expression");
        Consumer<Integer> exp33 = prefix -> System.out.println(prefix + "One param lambda expression");
        exp3.accept("AAAAA: ");
        exp33.accept(999999999);

        BiConsumer<String, String> exp4 = (prefix, postfix) -> System.out.println(prefix + " two params lambda expression " + postfix);
        BiConsumer<String, String> exp5 = (String prefix, String postfix) -> System.out.println(prefix + " two params lambda expression " + postfix);

        // Body and return type
        Function<String, String> exp6 = input -> {
            String someText = "No param lambda expression";
            return someText + input;
        };

        Function<List<Float>, Integer> fn = input -> {
            return input.size();
        };

        Car ferrari = new Ferrari();
        ferrari.drive(150);

        Car ghost = (accelerate) -> System.out.println("Ghost car " + accelerate);
        ghost.drive(1100);

    }

    @FunctionalInterface
    interface Car {
        void drive(int speed); /* {
            System.out.println("Ghost car " + accelerate);
        }
        */
    }

    public static class Ferrari implements Car {
        @Override
        public void drive(int speed) {
            System.out.println("Ferrari is driving with " + speed);
        }
    }
}
