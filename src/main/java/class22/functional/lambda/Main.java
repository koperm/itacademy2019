package class22.functional.lambda;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        Supplier<String> exp1 = () -> "No param lambda expression";
        Consumer<String> exp2 = (prefix) -> System.out.println(prefix + "One param lambda expression");
        Consumer<String> exp3 = prefix -> System.out.println(prefix + "One param lambda expression");
        BiConsumer<String, String> exp4 = (prefix, postfix) -> System.out.println(prefix + " two params lambda expression " + postfix);
        BiConsumer<String, String> exp5 = (String prefix, String postfix) -> System.out.println(prefix + " two params lambda expression " + postfix);

        // Body and return type
        Function<String, String> exp6 = input -> {
            String someText = "No param lambda expression";
            return someText + input;
        };

        // Lambda objects
        SimpleComparator myComparator = (val1, val2) -> val1 > val2;
        boolean result = myComparator.compare(2, 5);
    }

    @FunctionalInterface
    public interface SimpleComparator {
        boolean compare(int a1, int a2);
    }
}
