package class22.functional.method_ref;

public class Main {

    public static void main(String[] args) {
        // Static Method Reference
        Finder finder1 = MyClass::doFind;
        Finder finder111 = (str1, str2) -> MyClass.doFind(str1, str2);
        finder1.find("one", "two");

        // Instance Method References
        StringConverter stringConverter = new StringConverter();
        Deserializer des = stringConverter::convertToInt;
        Deserializer des1 = (v1) -> stringConverter.convertToInt(v1);
        Deserializer des2 = (v1) -> Integer.valueOf(v1);
        des.deserialize("some");

        StringConsumer stringConsumer = new StringConsumer();
        MyPrinter myPrinter1 = (s) -> {stringConsumer.consume(s);};
        MyPrinter myPrinter2 = s -> stringConsumer.consume(s);
        MyPrinter myPrinter3 = stringConsumer::consume;
        myPrinter3.print("some text");

        // Parameter Method Reference
        Finder finder2 = String::indexOf;
        Finder finder3 = (s1, s2) -> s1.indexOf(s2);
        finder2.find("ala", "a");

        // Constructor References
        Factory factory1 = String::new;
        factory1.create("ab");
    }

    interface MyPrinter {
        void print(String s);
    }

    interface Finder {
        int find(String s1, String s2);
    }

    interface Deserializer {
        int deserialize(String v1);
    }

    private static class MyClass {
        private static int doFind(String s1, String s2) {
            return s1.lastIndexOf(s2);
        }
    }

    private static class StringConverter {
        int convertToInt(String v1) {
            return Integer.valueOf(v1);
        }
    }

    private static class StringConsumer {
        void consume(String str) {
            System.out.println(str);
        }
    }

    interface Factory {
        String create(String vale);
    }

}
