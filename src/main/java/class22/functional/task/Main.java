package class22.functional.task;

import java.util.function.Consumer;
import java.util.function.Function;

public class Main {
    private static final String CONSUMER_INPUT = "consumer input";
    private static final Person PERSON = new Person("John", 34);
    private static final String MESSAGE = "message";

    public static void main(String[] args) {
        // TODO create lambda body (whatever you like) for every method below
        consumer((str) -> System.out.println(str));
        function((person) -> person.getAge());
        sendMail((msg) -> {
            return;
        });
    }

    static void consumer(Consumer<String> printer) {
        printer.accept(CONSUMER_INPUT);
    }

    static int function(Function<Person, Integer> calculateAge) {
        return calculateAge.apply(PERSON);
    }

    static void sendMail(ISender mailSender) {
        mailSender.send(MESSAGE);
    }

    @FunctionalInterface
    interface ISender {
        void send(String message);
    }


}
