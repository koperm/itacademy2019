package class22.functional.event_listener;

public class MyEvent {
    // description what happened
    private String description;

    public MyEvent(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
