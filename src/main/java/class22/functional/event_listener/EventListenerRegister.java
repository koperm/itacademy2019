package class22.functional.event_listener;

import com.google.common.collect.Lists;

import java.util.List;

public class EventListenerRegister {
    private List<IListener> listeners = Lists.newArrayList();

    public void addStateListener(IListener listener) {
        listeners.add(listener);
    }

    public List<IListener> getListeners() {
        return listeners;
    }
}
