package class22.functional.event_listener;

@FunctionalInterface
public interface IListener {
    void handleEvent(MyEvent event);
}

