package class22.functional.event_listener;

public class Main {
    public static void main(String[] args) {
        EventListenerRegister register = new EventListenerRegister();

        // Java 7
        register.addStateListener(new IListener() {
            public void handleEvent(MyEvent event) {
                // do something with event
                System.out.println("Java 7 handles event: " + event.getDescription());
            }
        });

        // Register preexisting instance
        IListener anonymousInstance = floating -> {
            // do something with event
            System.out.println("Java 8 handles event: " + floating.getDescription());
        };
        register.addStateListener(anonymousInstance);

        // Java 8
        register.addStateListener(event -> {
            // do something with event
            System.out.println("Java 8 handles event: " + event.getDescription());
        });

        /////////////////////////////////////////////////////

        // handle event
        MyEvent myEvent = new MyEvent("This is Event representing something horrible");
        for (IListener listener : register.getListeners()) {
            listener.handleEvent(myEvent);
        }
    }
}
