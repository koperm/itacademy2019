package class22.functional.effective_final;

public class Main {
    private int nonFinalInt = 1;
    private final int finalInt = 1;
    private static int staticInt = 3;

    public static void main(String[] args) {
        new Main().test();
    }

    void test() {
        int localNonFinalInt = 4;
        final int finalInt = 5;
        nonFinalInt = 6;
//        localNonFinalInt = 7; // TODO explain

        ITest testExp1 = () -> System.out.println("simple lambda" + nonFinalInt);
        ITest testExp2 = () -> System.out.println("simple lambda" + finalInt);
        ITest testExp3 = () -> System.out.println("simple lambda" + staticInt);
        ITest testExp4 = () -> System.out.println("simple lambda" + localNonFinalInt);
        ITest testExp5 = () -> System.out.println("simple lambda" + nonFinalInt);

        ITest testExp6 = () -> System.out.println("simple lambda" + this.finalInt);

    }

    @FunctionalInterface
    interface ITest {
        void testIt();
    }
}
