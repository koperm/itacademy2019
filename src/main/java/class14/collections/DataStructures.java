package class14.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DataStructures {
    public static void main(String[] args) {
        int[] arrayInts = new int[]{4, 8, 1, 5};
        arrayInts[0] = 67;

        for (int i : arrayInts) {
            System.out.println(i);
        }

//////////////////////////////////////////////////////////////////

        List<Integer> ints = new ArrayList<>();
        ints.add(5);
        ints.add(1);
        ints.add(8);
        ints.add(4);

        for (Integer i : ints) {
            System.out.println(i);
        }

        System.out.println(ints.get(2));

        ints.add(45);
//        ints.add("dfsda");    // TODO why?

        int integer = ints.get(0);

        List<String> strings = new LinkedList<>();
        strings.add("ad");

//////////////////////////////////////////////////////////////////////////////

        Set<String> setStrings = new HashSet<>();
        setStrings.add("fdsfad");
//        setStrings.get(2);    // TODO why?


    }
}
