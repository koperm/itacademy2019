package class14.collections.task;

import java.util.List;

public class MainClass {
    public static void main(String[] args) {
        Person father = new Person("father", 45);
        Person mother = new Person("mother", 43);
        Person child = new Person("child", 20);

        father.setRelatives(List.of(mother, child));
        mother.setRelatives(List.of(father, child));
        child.setRelatives(List.of(father, mother));

        System.out.println(father.toString());
    }
}
