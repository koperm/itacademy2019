package class14.collections.task;

import java.util.List;

public class Person {
    private String name;
    private int age;
    private List<Person> relatives;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Person> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<Person> relatives) {
        this.relatives = relatives;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", relatives=" + relatives +    // TODO infinitive loop cause
//                ", relatives=" + convertRelatives(relatives) +
                '}';
    }

    private String convertRelatives(List<Person> relatives) {
        String out = "";
        for (Person relative : relatives) {
            out = out + relative.name + relative.age;
        }

        return out;
    }
}
