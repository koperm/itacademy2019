package class6.classes;

import class6.classes.subpckg.SubClassOne;
//import class6.classes.subpckg.SubClassTwo;    // TODO why?

public class OuterClass {
    public int x;
    int y;
    private int z;

    public OuterClass(int x) {
        this.x = x;
    }

    OuterClass() {
    }

    private OuterClass(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void methodOne() {
    }

    void methodTwo() {
    }

    private void methodThree() {
    }

    public static void main(String[] args) {
//        SubClassOne subClassOne = new SubClassOne();  // TODO why?
        SubClassOne subClassOne1 = new SubClassOne(5);
//        SubClassOne subClassOne2 = new SubClassOne(5, 10);    // TODO why?
/*
        // TODO why?
        SubClassTwo subClassTwo = new SubClassTwo();
        SubClassTwo subClassTwo1 = new SubClassTwo(5);
        SubClassTwo subClassTwo2 = new SubClassTwo(5, 10);
*/

        subClassOne1.methodOne();
//        subClassOne1.methodTwo();       // TODO why
//        subClassOne1.methodThree();    // TODO why?
        int x = subClassOne1.x;
//        int y = subClassOne1.y;     // TODO why
//        int z = subClassOne1.z;    // TODO why
/*
        // TODO why
        subClassTwo.methodOne();
        subClassTwo.methodTwo();
        subClassTwo.methodThree();
        int x1 = subClassTwo.x;
        int y1 = subClassTwo.y;
        int z1 = subClassTwo.z;
*/

    }
}
