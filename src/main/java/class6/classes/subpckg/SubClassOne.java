package class6.classes.subpckg;

public class SubClassOne {

    public int x;
    int y;
    private int z;

    public SubClassOne(int x) {
        this.x = x;
    }

    SubClassOne() {
    }

    private SubClassOne(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void methodOne() {
    }

    void methodTwo() {
    }

    private void methodThree() {
    }

    public static void main(String[] args) {
        SubClassOne subClassOne = new SubClassOne();
        SubClassOne subClassOne1 = new SubClassOne(5);
        SubClassOne subClassOne2 = new SubClassOne(5, 10);

        SubClassTwo subClassTwo = new SubClassTwo();
        SubClassTwo subClassTwo1 = new SubClassTwo(5);
//            SubClassTwo subClassTwo2 = new SubClassTwo(5, 10);// TODO why?

        subClassOne.methodOne();
        subClassOne.methodTwo();
        subClassOne.methodThree();
        int x = subClassOne.x;
        int y = subClassOne.y;
        int z = subClassOne.z;

        subClassTwo.methodOne();
        subClassTwo.methodTwo();
//            subClassTwo.methodThree();// TODO why?
        int x1 = subClassTwo.x;
        int y1 = subClassTwo.y;
//            int z1 = subClassTwo.z;   // TODO why?

    }
}

