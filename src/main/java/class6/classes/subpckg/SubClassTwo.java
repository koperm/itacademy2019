package class6.classes.subpckg;

class SubClassTwo {
    public int x;
    int y;
    private int z;

    public SubClassTwo(int x) {
        this.x = x;
    }

    SubClassTwo() {
    }

    private SubClassTwo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void methodOne() {
    }

    void methodTwo() {
    }

    private void methodThree() {
    }

    public static void main(String[] args) {
        SubClassOne subClassOne = new SubClassOne();
        SubClassOne subClassOne1 = new SubClassOne(5);
//        SubClassOne subClassOne2 = new SubClassOne(5, 10);    // TODO why?

        SubClassTwo subClassTwo = new SubClassTwo();
        SubClassTwo subClassTwo1 = new SubClassTwo(5);
        SubClassTwo subClassTwo2 = new SubClassTwo(5, 10);

        subClassOne.methodOne();
        subClassOne.methodTwo();
//        subClassOne.methodThree();    // TODO why?
        int x = subClassOne.x;
        int y = subClassOne.y;
//        int z = subClassOne.z;    // TODO why

        subClassTwo.methodOne();
        subClassTwo.methodTwo();
        subClassTwo.methodThree();
        int x1 = subClassTwo.x;
        int y1 = subClassTwo.y;
        int z1 = subClassTwo.z;

    }
}
