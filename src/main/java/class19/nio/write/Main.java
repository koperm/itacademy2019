package class19.nio.write;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        String dir = "src\\main\\resources\\";
        String path = dir + "testFile.txt";

        // write/override BufferedWriter
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write("Hello");
        writer.close();

        // append BufferedWriter
        BufferedWriter writer2 = new BufferedWriter(new FileWriter(path, true));
        writer2.append(' ');
        writer2.append("World");
        writer2.close();

        // PrintWriter
        FileWriter fileWriter = new FileWriter(path);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print("Some String");
        printWriter.printf("My name is %s and I am %d years old", "John", 45);
        printWriter.close();

        // binary data with FileOutputStream
        FileOutputStream outputStream = new FileOutputStream(path);
        byte[] strToBytes = "Hello".getBytes();
        outputStream.write(strToBytes);
        outputStream.close();

        // DataOutputStream (primitive Java types with special Modified UTF-8 encoding)
        DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path)));
        outStream.writeUTF("Hello");
        outStream.close();
        // read from DataOutputStream
        DataInputStream reader = new DataInputStream(new FileInputStream(path));
        String result = reader.readUTF();
        reader.close();

        // RandomAccessFile write
        RandomAccessFile randomWriter = new RandomAccessFile(path, "rw");
        long position = 4;
        randomWriter.seek(position);
        randomWriter.writeInt(789);
        randomWriter.close();

        // RandomAccessFile read
        RandomAccessFile reader2 = new RandomAccessFile(path, "r");
        reader2.seek(position);
        int result2 = reader2.readInt();
        reader2.close();

        // Write with Java 7
        String str = "Hello";
        Path filePath = Paths.get(path);
        byte[] strToBytes2 = str.getBytes();
        Files.write(filePath, strToBytes2);
        String read = Files.readAllLines(filePath).get(0);

        // Write to temporary file
        String toWrite = "Hello";
        File tmpFile = File.createTempFile("test", ".tmp");
        FileWriter writer3 = new FileWriter(tmpFile);
        writer3.write(toWrite);
        writer3.close();
    }
}
