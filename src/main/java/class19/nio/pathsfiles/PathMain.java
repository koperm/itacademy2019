package class19.nio.pathsfiles;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathMain {
    public static void main(String[] args) throws IOException {
        // Examples taken from https://www.baeldung.com/java-nio-2-path

        // TODO escaping character
        Path path1 = Paths.get("pathstring");
        Path path2 = Paths.get("/articles", "baeldung", "blabl", "fasdfa");
        Path path3 = Paths.get("/articles/baeldung/");

        path3.getFileName();
        path3.getName(0);
        path3.subpath(0, 1);
        path3.getParent();
        path3.getRoot();

        // Normalization
        Path path4 = Paths.get("/baeldung/./articles");
        Path path5 = Paths.get("/baeldung/authors/../articles");
        Path path6 = Paths.get("/baeldung/articles");

        path4.normalize();

        // Conversion // TODO URI explanation
        Path path7 = Paths.get("baeldung/articles");
        path7.toUri();
        path7.toAbsolutePath();
        // path7.toRealPath();  // TODO run in debug

        // Joining
        Path path8 = Paths.get("/baeldung/articles");
        path8.resolve("java");
        path8.resolve("articles/java");
        path8.resolve("/baeldung/projects/java");
        path8.resolve("baeldung/projects/java");
        path8.resolve("C:\\baeldung\\projects\\java");

        // Relativizing
        Path p1 = Paths.get("/articles/dir1/dir2");
        Path p2 = Paths.get("/articles/dir1/dir3/dir4");
        p1.relativize(p2);
        p2.relativize(p1);
    }
}
