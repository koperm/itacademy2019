package class19.nio.pathsfiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileMain {
    private static String HOME = System.getProperty("user.home");
    private static Path HOME_PATH = Paths.get(HOME, "Desktop/NIO");
    private static Path FILE_PATH = HOME_PATH.resolve("testFile.txt");
    private static Path FILE_PATH_1 = HOME_PATH.resolve("testFile1.txt");


    public static void main(String[] args) throws IOException {
        // Examples taken from https://www.baeldung.com/java-nio-2-file-api

        Files.exists(HOME_PATH);

        Files.notExists(HOME_PATH.resolve("inexistent_file.txt"));

        Files.isRegularFile(HOME_PATH);

        Files.isReadable(HOME_PATH);
        Files.isWritable(HOME_PATH);
        Files.isExecutable(HOME_PATH);

        Files.isSameFile(HOME_PATH, Paths.get(HOME));

        Files.createFile(FILE_PATH);

        Files.createDirectory(HOME_PATH.resolve("newCatalog"));   // exception if path is not complete
        Files.createDirectories(HOME_PATH.resolve("newCatalog"));

        Files.createTempFile(HOME_PATH, null, null);
        Files.createTempFile(null, null);

        Files.delete(FILE_PATH);
        Files.deleteIfExists(FILE_PATH);

        Files.move(FILE_PATH, FILE_PATH_1);
        Files.move(FILE_PATH, FILE_PATH_1, StandardCopyOption.REPLACE_EXISTING);

        Files.copy(FILE_PATH, FILE_PATH_1);
        Files.copy(FILE_PATH, FILE_PATH_1, StandardCopyOption.REPLACE_EXISTING);

    }
}
