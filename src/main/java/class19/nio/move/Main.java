package class19.nio.move;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        String dir = "src\\main\\resources\\";
        String dstDir = dir + "dst\\";
        String srcPath = dir + "testFile.txt";
        String dstPath = dstDir + "testFile.txt";   // TODO replace with dir and check

        // Java 6
        File fileToMove = new File(srcPath);
        fileToMove.renameTo(new File(dstPath));

        // Java 7
        Path fileToMovePath = Files.createFile(Paths.get(srcPath));
        Path targetPath = Paths.get(dstDir);
        Files.move(fileToMovePath, targetPath.resolve(fileToMovePath.getFileName()));

        // Guava
        File fileToMove2 = new File(srcPath);
        File destDir = new File(dstDir);
        File targetFile = new File(destDir, fileToMove2.getName());

        com.google.common.io.Files.move(fileToMove2, targetFile);

        // Apache
        FileUtils.moveFile(
                FileUtils.getFile(srcPath),
                FileUtils.getFile(dstPath)
        );

        FileUtils.moveFileToDirectory(
                FileUtils.getFile(srcPath),
                FileUtils.getFile(dstDir),
                true);
    }
}
