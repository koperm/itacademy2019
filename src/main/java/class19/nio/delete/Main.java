package class19.nio.delete;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        String dir = "src\\main\\resources\\";

        // Java 6
        new File(dir + "\\file1").delete();

        // Java 7
        Files.delete(Paths.get(dir, "file2"));

        // Apache
        FileUtils.deleteQuietly(Path.of(dir, "file3").toFile());

    }
}
