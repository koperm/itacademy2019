package class19.nio.create;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class Main {
    public static void main(String[] args) throws IOException {
        Path dir = Path.of("src\\main\\resources\\");
        String path = dir + "testFile.txt";

        // Java 6
        new File(dir.toFile(), "file1").createNewFile();

        // Java 7
        java.nio.file.Files.createFile(dir.resolve("file2"));

        // Guava
        Files.touch(dir.resolve("file3").toFile());

        // Apache
        FileUtils.touch(dir.resolve("file4").toFile());
    }
}
