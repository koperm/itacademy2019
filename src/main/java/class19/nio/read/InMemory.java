package class19.nio.read;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class InMemory {
    public static void main(String[] args) throws IOException {
        String path = "src\\main\\resources\\testFile.txt";

        List<String> strings = Files.readLines(new File(path), Charsets.UTF_8);
        List<String> strings1 = FileUtils.readLines(new File(path), Charsets.UTF_8);
    }
}
