package class19.nio.read;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Streaming {
    public static void main(String[] args) throws IOException {
        String path = "src\\main\\resources\\testFile.txt";

        fileInputStream(path);
        fileInputStreamTryWithRes(path);
        fileUtils(path);
    }

    private static void fileInputStream(String path) throws IOException {
        FileInputStream inputStream = null;
        Scanner sc = null;

        try {
            inputStream = new FileInputStream(path);
            sc = new Scanner(inputStream, StandardCharsets.UTF_8);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                System.out.println(line);
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }
    }

    private static void fileInputStreamTryWithRes(String path) throws IOException {

//        try (FileInputStream inputStream = new FileInputStream(path);
//             Scanner sc = new Scanner(inputStream, StandardCharsets.UTF_8)) {
        try (Scanner sc = new Scanner(new FileInputStream(path), "UTF-8")) {

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                System.out.println(line);
            }
        }
    }

    private static void fileUtils(String path) throws IOException {
        LineIterator it = FileUtils.lineIterator(new File(path), "UTF-8");
        try {
            while (it.hasNext()) {
                String line = it.nextLine();
                System.out.println(line);
            }
        } finally {
            LineIterator.closeQuietly(it);
        }
    }
}
