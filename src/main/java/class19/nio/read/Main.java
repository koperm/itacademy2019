package class19.nio.read;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        String dir = "src\\main\\resources\\";
        String path = dir + "testFile.txt";

        // BufferedReader
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String currentLine = reader.readLine();
        reader.close();

        // Scanner
        Scanner scanner = new Scanner(new File(path));
        scanner.useDelimiter(" ");
        scanner.hasNext();
        String result = scanner.next();
        scanner.close();

        // Tokenizer "Hello 1"
        FileReader reader2 = new FileReader(path);
        StreamTokenizer tokenizer = new StreamTokenizer(reader2);
        // token 1
        tokenizer.nextToken();
        System.out.println(StreamTokenizer.TT_WORD + " " + tokenizer.ttype);
        System.out.println(tokenizer.sval);
        // token 2    
        tokenizer.nextToken();
        System.out.println(StreamTokenizer.TT_NUMBER + " " + tokenizer.ttype);
        System.out.println(tokenizer.nval);
        // token 3
        tokenizer.nextToken();
        System.out.println(StreamTokenizer.TT_EOF + " " + tokenizer.ttype);
        reader2.close();

        // DataOutputStream (primitive Java types with special Modified UTF-8 encoding)
        DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path)));
        outStream.writeUTF("Hello");
        outStream.close();
        // read from DataOutputStream
        DataInputStream reader3 = new DataInputStream(new FileInputStream(path));
        String result1 = reader3.readUTF();
        reader.close();

        // UTF "青空"
        BufferedReader reader4 = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
        String line = reader4.readLine();
        reader4.close();

        // Read into String
        BufferedReader reader5 = new BufferedReader(new FileReader(path));
        StringBuilder builder = new StringBuilder();
        String cline = reader5.readLine();
        while (cline != null) {
            builder.append(cline);
            builder.append("n");
            cline = reader5.readLine();
        }
        reader5.close();

        // Java 7 (small file)
        String read = Files.readAllLines(Paths.get(path)).get(0);

        // Java 7 (large file)
        BufferedReader reader6 = Files.newBufferedReader(Paths.get(path));
        String line1 = reader6.readLine();


    }
}
