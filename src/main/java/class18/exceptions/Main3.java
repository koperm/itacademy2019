package class18.exceptions;

import java.io.IOException;
import java.util.List;

public class Main3 {

    private static final List<Throwable> THROWABLES = List.of(
            new MyCompiletimeExc("MyCompiletimeExc!!!"),
            new MyRuntimeExc("MyRuntimeExc!!!"),
            new IOException("IOException!!!"),
            new NullPointerException("NullPointerException!!!"),
            new RuntimeException("RuntimeException!!!"),
            new Exception("Exception!!!"),
            new Error("Error!!!"),
            new Throwable("Throwable!!!")
    );

    private static Throwable throwable;

    public static void main(String[] args) {

        for (Throwable exc : THROWABLES) {
            throwable = exc;
            System.out.println("Execution for " + throwable.getClass().getSimpleName());
            outerMethod();
            System.out.println();
        }
    }

    private static void outerMethod() {
        try {
            innerMethod();
            System.out.println("\touterMethod after method");
        } catch (RuntimeException ex) {
            System.out.println("\touterMethod catch RuntimeException");
        } catch (Exception ex) {
            System.out.println("\touterMethod catch Exception");
        } catch (Throwable ex) {
            System.out.println("\touterMethod catch Throwable");
        } finally {
            System.out.println("\touterMethod finally");
        }

        System.out.println("\touterMethod after try-catch");
    }

    private static void innerMethod() throws Throwable {
        try {
            throwingMethod();
        } catch (MyRuntimeExc ex) {
            System.out.println("\t\tinnerMethod catch MyRuntimeExc");
        } catch (MyCompiletimeExc ex) {
            System.out.println("\t\tinnerMethod catch MyCompiletimeExc");
        } finally {
            System.out.println("\t\tinnerMethod finally");
        }

        System.out.println("\t\tinnerMethod after try-catch");
    }

    private static void throwingMethod() throws Throwable {
        throw throwable;
    }

    private static class MyRuntimeExc extends RuntimeException {
        public MyRuntimeExc(String message) {
            super(message);
        }
    }

    private static class MyCompiletimeExc extends Exception {
        public MyCompiletimeExc(String message) {
            super(message);
        }
    }

}
