package class18.exceptions;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {
        case1();
        case2();
        case3();
        case4();
    }

    private static void case4() {
        try {
            m2();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            throw new RuntimeException();
//            System.out.println("Case4");  // TODO
        }
    }

    private static void case3() {
        try {
            m1();
        } finally {
            System.out.println("Case3");
        }
    }

    private static void case2() {
        try {
            m2();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Case2 printout");
        }
    }

    private static void case1() {
        try {
            m2();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

        m1();
    }

    static void m1() {
        throw new RuntimeException();
    }

    static void m2() throws IOException, SQLException {
        throw new SQLException();
    }



}
