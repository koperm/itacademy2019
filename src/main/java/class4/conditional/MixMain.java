package class4.conditional;

public class MixMain {
    public static void main(String[] args) {
        int t = 10;

        do {
//            t += 2; // t = t + 2;
            t++;    // t = t + 1;

            if (t == 15) {
                continue;
            }

            for (int i = 0; i < 3; i++) {
                if (i == 1) {
                    break;
                }
                System.out.println(i);
            }

            System.out.println(t);
        } while (t < 20);
    }
}
