package class4.conditional;

/*
* Pseudo code:
for i = <0,20>
	If i <=10
		Print only even number (modulo operator may be helpful, e.g. i % 2 ==0)
	Else if i <= 20
		Print every third number (again modulo: i % 3 == 0)
		If (i == 13)
			Print “That has changed”
		If (i ==15)
			for j = <40,50>
				Print j
* */
public class TaskLoop {
    public static void main(String[] args) {

        for (int i = 0; i <= 20; i++) {
//            System.out.println(i <= 10 & i % 2 == 0);
            if (i <= 10 && i % 2 == 0) {
                System.out.println(i);
            } else if (i <= 20) {
                if (i % 3 == 0) {
                    System.out.println(i);
                }

                if (i % 3 == 0 && i == 13) {
                    System.out.println("That has changed");
                }

                if (i == 15) {
                    for (int j = 40; j <= 50; j++) {
                        System.out.println(j);
                    }
                }
            }
        }
    }
}
