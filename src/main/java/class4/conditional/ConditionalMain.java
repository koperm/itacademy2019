package class4.conditional;

public class ConditionalMain {
    public static void main(String[] args) {
        boolean isFlueAnnoying = true;
        boolean isHeadAcheAnnoying = false;

        if (isFlueAnnoying) {
            System.out.println("Sick leave");
        }

        if (!isFlueAnnoying) {
            System.out.println("go to work");
        }

        if (isFlueAnnoying || isHeadAcheAnnoying) {
            System.out.println("you are probably sick");
        }

        if (!(isFlueAnnoying && isHeadAcheAnnoying)) {
            System.out.println("you are definitely mind sick");
        }

        if (!isFlueAnnoying || !isHeadAcheAnnoying) {
            System.out.println("deMorgan law");
        }

        if (!isFlueAnnoying && !(isFlueAnnoying || isHeadAcheAnnoying)) {
            System.out.println("mind blow");

        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // 2.9e2 = 2.9 * 10^2
        int g = 10^2;
        System.out.println(g);  // 8,  XOR operation

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        int age = 40;

        if (age < 18) {
            System.out.println("You are not allowed to buy it");
        } else if (age == 18) {
            System.out.println("checking ID");
        } else {
            System.out.println("here you are");
        }

        switch (age) {
            case 20: {
                System.out.println("discount 20");
                break;
            }
            case 40: {
                System.out.println("discount 40");
            }
            default:
                System.out.println("sorry no discount");
        }

        String name = "Joe";
        switch (name) {
            case "Juliet": {
                System.out.println("discount 20");
                break;
            }
            case "Joe": {
                System.out.println("discount 40");
            }
            default:
                System.out.println("sorry no discount");
        }

        if (age != 5) {
            System.out.println("whatever");
        } else {
            System.out.println("so what");
        }
    }
}
