package class4.conditional;

public class LoopMain {
    public static void main(String[] args) {

        int i = 0;

        for (; i < 10; i = i + 1) {
            System.out.println(i);
        }

        for (; i < 20; i++) {
            System.out.println(i);
        }

        for (int age = 0; age < 10; age++) {
            System.out.println(age);
        }

        while (i < 30) {
            i++;
            System.out.println(i);
        }

        do {
            System.out.println(i);
            i++;
        } while (i < 40);


        System.out.println("Here we go");
        System.out.println(i);
        System.out.println(i++);
        System.out.println(++i);
        System.out.println(i--);
        System.out.println(--i);
        System.out.println(-i);
    }
}
