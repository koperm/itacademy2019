package class16.iteratorscomparators.comparing.comparator;

import class16.iteratorscomparators.comparing.Author;

import java.util.Comparator;

public class CustomComparator implements Comparator<Author> {
    public int compare(Author a1, Author a2) {
        if (a1.age == a2.age && a1.firstName.equals(a2.firstName) && a1.bookName.equals(a2.bookName)) {
            return 0;
        } else if (a1.age > a2.age && a1.firstName.compareTo(a2.firstName) > 0 && a1.bookName.compareTo(a2.bookName) > 0) {
            return 1;
        } else {
            return -1;
        }
    }
}