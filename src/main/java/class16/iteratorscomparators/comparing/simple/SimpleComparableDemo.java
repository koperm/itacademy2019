package class16.iteratorscomparators.comparing.simple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SimpleComparableDemo {
    public static void main(String[] args) {
        /*
         * Integer class implements Comparable
         * Interface so we can use the sort method
         */
        int[] arr = {11, 55, 22, 0, 89};
        Arrays.sort(arr);
        System.out.println("Sorted Int Array: ");
        System.out.println(Arrays.toString(arr));

        /*
         * String class implements Comparable
         * Interface so we can use the sort method
         */
        System.out.println("Sorted String Array: ");
        String[] names = {"Steve", "Ajeet", "Kyle"};
        Arrays.sort(names);
        System.out.println(Arrays.toString(names));

        /*
         * String class implements Comparable
         * Interface so we can use the sort method
         */
        System.out.println("Sorted List: ");
        List<String> fruits = new ArrayList<>();
        fruits.add("Orange");
        fruits.add("Banana");
        fruits.add("Apple");
        fruits.add("Guava");
        fruits.add("Grapes");
        Collections.sort(fruits);

        for (String s : fruits)
            System.out.println(s + ", ");

        Collections.sort(fruits, Collections.reverseOrder());
        System.out.println("Sorted List in descending order: ");

        for (String s : fruits)
            System.out.println(s + ", ");

        Collections.reverse(fruits);
        System.out.println("Flip order: ");

        for (String s : fruits)
            System.out.println(s + ", ");
    }
}
