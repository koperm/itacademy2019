package class16.iteratorscomparators.comparing;

public class Author implements Comparable<Author> {

    public String firstName;
    public String bookName;
    public int age;

    public Author(String firstName, String bookName, int age) {
        this.firstName = firstName;
        this.bookName = bookName;
        this.age = age;
    }

    @Override
    /**
     * This is where we write the logic to sort. This method sort
     * by the first name
     */
    public int compareTo(Author author) {
        /*
         * Sorting by first name. compareTo should return < 0 if this(keyword)
         * is supposed to be less than author, > 0 if this is supposed to be
         * greater than object author and 0 if they are supposed to be equal.
         */
        return this.firstName.compareTo(author.firstName);
    }

    @Override
    public String toString() {
        return "Author{" +
                "firstName='" + firstName + '\'' +
                ", bookName='" + bookName + '\'' +
                ", age=" + age +
                '}';
    }
}
