package class16.iteratorscomparators.comparing.comparable;

import class16.iteratorscomparators.comparing.Author;

import java.util.ArrayList;
import java.util.Collections;

public class AuthorComparableDemo {

    public static void main(String args[]) {
        // List of objects of Author class
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(new Author("Henry", "Tropic of Cancer", 45));
        authors.add(new Author("Nalo", "Brown Girl in the Ring", 56));
        authors.add(new Author("Frank", "300", 65));
        authors.add(new Author("Deborah", "Sky Boys", 51));
        authors.add(new Author("George R. R.", "A Song of Ice and Fire", 62));

        /*
         * Sorting the list using Collections.sort() method, we
         * can use this method because we have implemented the
         * Comparable interface in our user defined class Author
         */
        Collections.sort(authors);
        for (Author author : authors) {
            System.out.println(author);
        }
    }
}
