package class16.iteratorscomparators.iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class ConcurrentModificationException {
    public static void main(String args[]) {
        ArrayList<String> books = new ArrayList<>();
        books.add("C");
        books.add("Java");
        books.add("Cobol");

        for (String book : books) {
            System.out.println(book);
            //We are adding/removing element while iterating list what causes exception
            books.add("C++");
            // or
            books.remove("Java");
        }

        for (Iterator<String> it = books.iterator(); it.hasNext(); ) {
            String value = it.next();
            it.remove();    // it works fine
        }

    }

}
