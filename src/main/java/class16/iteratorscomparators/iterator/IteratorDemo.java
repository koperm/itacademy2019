package class16.iteratorscomparators.iterator;

import class15.maps.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class IteratorDemo {

    public static void main(String args[]) {
        List<String> names = new ArrayList<>();
        names.add("Chaitanya");
        names.add("Steve");
        names.add("Jack");

        for (int i = 0; i < names.size(); i++) {
            System.out.println(names.get(i));
        }

        for (String name : names) {
            System.out.println(name);
        }

        for (Iterator<String> it = names.iterator(); it.hasNext(); ) {
            System.out.println(it.next());
        }

        Iterator<String> it2 = names.iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            System.out.println(next);
        }

        Map<String, Person> stringPersonHashMap = new HashMap<>();
        for (Map.Entry<String, Person> entry : stringPersonHashMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        for (Person person : stringPersonHashMap.values()) {
            System.out.println(person);
        }

        for (String key : stringPersonHashMap.keySet()) {
            System.out.println(key);
        }
    }


}
