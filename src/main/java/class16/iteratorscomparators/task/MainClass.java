package class16.iteratorscomparators.task;

import java.util.List;

public class MainClass {
    public static void main(String[] args) {
        task1();
        task2();
    }

    private static void task1() {
        Person johnSmith = new Person("John", "Smith", 40);
        Person aliceSmith = new Person("Alice", "Smith", 35);
        Person maxKennedy = new Person("Max", "Smith", 10);
        Person jackCandy = new Person("Jack", "Smith", 15);

        johnSmith.addFamilyMember(aliceSmith);
        johnSmith.addFamilyMember(maxKennedy);
        johnSmith.addFamilyMember(jackCandy);

        List<Person> familySorted = johnSmith.getFamilySorted();
        System.out.println(familySorted);
        System.out.println(johnSmith.getFamilyTreeSet());
    }

    private static void task2() {
        Person johnSmith = new Person("John", "Smith", 40);
        Person aliceSmith = new Person("Alice", "Smith", 35);
        Person maxKennedy = new Person("Max", "Kennedy", 10);
        Person jackCandy = new Person("Jack", "Candy", 15);

        List<Person> allPeople = List.of(johnSmith, aliceSmith, maxKennedy, jackCandy);

        for (Person person : allPeople) {
            for (Person relative : allPeople) {
                if (person.equals(relative)) {
                    continue;
                }

                if (person.getSurname().equals(relative.getSurname())) {
                    person.addFamilyMember(relative);
                }
            }
        }

        System.out.println(johnSmith);
        System.out.println(aliceSmith);
        System.out.println(maxKennedy);
    }
}
