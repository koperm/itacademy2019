package class16.iteratorscomparators.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Person {
    private String name;
    private String surname;
    private int age;
    private SortedSet<Person> familyTreeSet = new TreeSet<>(new PersonAgeComparator());
    private Set<Person> familyHashSet = new HashSet<>();

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    void addFamilyMember(Person relative) {
        familyTreeSet.add(relative);
        familyHashSet.add(relative);
    }

    public SortedSet<Person> getFamilyTreeSet() {
        return familyTreeSet;
    }

    public List<Person> getFamilySorted() {
        List<Person> list = new ArrayList(familyHashSet);
        Collections.sort(list, new PersonAgeComparator());
        return list;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
//                ", relatives=" + familyTreeSet +    // TODO infinitive loop cause
                ", relatives=" + convertRelatives(familyTreeSet) +
                '}';
    }

    private String convertRelatives(Set<Person> relatives) {
        String out = "";
        for (Person relative : relatives) {
            out = out + relative.name + relative.age;
        }

        return out;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, age);
    }
}
