package class27.reflection;

class ReflectPojo {
    private String text;

    public ReflectPojo() {
        text = "Initial text";
    }

    public void method1() {
        System.out.println("No-arg, public method. My text:" + text);
    }

    public void method2(int number) {
        System.out.println("int arg = " + number + ", public method. My text:" + text);
    }

    private void method3() {
        System.out.println("No-arg, private method invoked My text:" + text);
    }
}
