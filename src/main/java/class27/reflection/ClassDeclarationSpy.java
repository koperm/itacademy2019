package class27.reflection;

import java.io.InterruptedIOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.security.Identity;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentNavigableMap;

import static java.lang.System.out;

public class ClassDeclarationSpy {
    // Examples taken from: https://docs.oracle.com/javase/tutorial/reflect/class/classModifiers.html

    public static void main(String... args) throws ClassNotFoundException {
        spy(ConcurrentNavigableMap.class); // public interface ConcurrentNavigableMap<K,V> extends ConcurrentMap<K,V>, NavigableMap<K,V>
        spy(InterruptedIOException.class);
        spy(Identity.class);
        spy(String.class.getCanonicalName());
        spy("[Ljava.lang.String;");
//        spy(int.class.getCanonicalName());    // TODO
        spy(int.class);
        spy(int[].class);

    }

    private static void spy(String arg) throws ClassNotFoundException {
        spy(Class.forName(arg));
    }

    private static void spy(Class<?> c) {
        out.format("Class:%n  %s%n%n", c.getCanonicalName());
        out.format("Modifiers:%n  %s%n%n", Modifier.toString(c.getModifiers()));

        spyTypeParameters(c);
        spyImplementedInterfaces(c);
        spyInheritance(c);
        spyAnnotations(c);
    }

    private static void spyTypeParameters(Class<?> c) {
        out.format("Type Parameters:%n");
        TypeVariable[] tv = c.getTypeParameters();
        if (tv.length != 0) {
            out.format("  ");
            for (TypeVariable t : tv) {
                out.format("%s ", t.getName());
                // TODO extend for generics
            }
            out.format("%n%n");
        } else {
            out.format("  -- No Type Parameters --%n%n");
        }
    }

    private static void spyImplementedInterfaces(Class<?> c) {
        out.format("Implemented Interfaces:%n");
        Type[] intfs = c.getGenericInterfaces();
        if (intfs.length != 0) {
            for (Type intf : intfs)
                out.format("  %s%n", intf.toString());
            out.format("%n");
        } else {
            out.format("  -- No Implemented Interfaces --%n%n");
        }
    }

    private static void spyInheritance(Class<?> c) {
        out.format("Inheritance Path:%n");
        List<Class> l = new ArrayList<Class>();
        printAncestor(c, l);
        if (l.size() != 0) {
            for (Class<?> cl : l)
                out.format("  %s%n", cl.getCanonicalName());
            out.format("%n");
        } else {
            out.format("  -- No Super Classes --%n%n");
        }
    }

    private static void spyAnnotations(Class<?> c) {
        out.format("Annotations:%n");
        Annotation[] ann = c.getAnnotations();

        if (ann.length != 0) {
            for (Annotation a : ann)
                out.format("  %s%n", a.toString());
            out.format("%n");
        } else {
            out.format("  -- No Annotations --%n%n");
        }
    }

    private static void printAncestor(Class<?> c, List<Class> l) {
        Class<?> ancestor = c.getSuperclass();
        if (ancestor != null) {
            l.add(ancestor);
            printAncestor(ancestor, l);
        }
    }
}