package class27.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.nio.channels.ReadableByteChannel;

import static java.lang.System.out;

enum ClassMember {
    CONSTRUCTOR,
    FIELD,
    METHOD,
    CLASS,
    ALL
}

public class ClassSpy {
    // Examples taken from: https://docs.oracle.com/javase/tutorial/reflect/class/classMembers.html

    public static void main(String... args) {
        spy(ClassCastException.class, "CONSTRUCTOR");
        spy(ReadableByteChannel.class, "METHOD");
        spy(ClassMember.class, "FIELD", "METHOD");
    }

    private static void spy(Class<?> c, String... members) {
        out.format("Class:%n  %s%n%n", c.getCanonicalName());

        Package p = c.getPackage();
        out.format("Package:%n  %s%n%n", (p != null ? p.getName() : "-- No Package --"));

        for (int i = 0; i < members.length; i++) {
            switch (ClassMember.valueOf(members[i])) {
                case CONSTRUCTOR:
                    printMembers(c.getConstructors(), "Constructor");
                    break;
                case FIELD:
                    printMembers(c.getFields(), "Fields");
                    break;
                case METHOD:
                    printMembers(c.getMethods(), "Methods");
                    break;
                case CLASS:
                    printClasses(c);
                    break;
                case ALL:
                    printMembers(c.getConstructors(), "Constuctors");
                    printMembers(c.getFields(), "Fields");
                    printMembers(c.getMethods(), "Methods");
                    printClasses(c);
                    break;
                default:
                    assert false;
            }
        }
    }

    private static void printMembers(Member[] mbrs, String s) {
        out.format("%s:%n", s);
        for (Member mbr : mbrs) {
            if (mbr instanceof Field) {
                out.format("  %s%n", ((Field) mbr).toGenericString());
                out.format("  Is array: %s%n", mbr.getDeclaringClass().isArray());
            } else if (mbr instanceof Constructor) {
                out.format("  %s%n", ((Constructor) mbr).toGenericString());
            } else if (mbr instanceof Method) {
                out.format("  %s%n", ((Method) mbr).toGenericString());
            }
        }

        if (mbrs.length == 0) {
            out.format("  -- No %s --%n", s);
        }

        out.format("%n");
    }

    private static void printClasses(Class<?> c) {
        out.format("Classes:%n");
        Class<?>[] clss = c.getClasses();

        for (Class<?> cls : clss) {
            out.format("  %s%n", cls.getCanonicalName());
        }

        if (clss.length == 0) {
            out.format("  -- No member interfaces, classes, or enums --%n");
        }

        out.format("%n");
    }
}
