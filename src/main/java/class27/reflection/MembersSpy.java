package class27.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MembersSpy {
    // Examples taken from: https://www.geeksforgeeks.org/reflection-in-java/

    public static void main(String[] args) throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {

//        Class<ReflectPojo> reflectPojoClass = ReflectPojo.class;
//        Constructor<ReflectPojo> constructor1 = reflectPojoClass.getConstructor();

        Constructor constructor = Class.forName("class27.reflection.ReflectPojo").getConstructor();
        System.out.println("The name of constructor is " + constructor.getName());

        Object instance = constructor.newInstance();

        Class cls = instance.getClass();
        System.out.println("The name of class of instance is " + cls.getName());

        System.out.println("The public methods of class are : ");
        Method[] methods = cls.getMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
        }

        Method methodWithIntArg = cls.getDeclaredMethod("method2", int.class);
        methodWithIntArg.invoke(instance, 19);  // reflectPojoInstance.method2(19)

        Method methodWoArg = cls.getDeclaredMethod("method1");
        methodWoArg.invoke(instance);

        Method privateMethodWoArg = cls.getDeclaredMethod("method3");
        privateMethodWoArg.setAccessible(true);
        privateMethodWoArg.invoke(instance);

        Field field = cls.getDeclaredField("text");
        field.setAccessible(true);  // TODO try with different access modifiers
        field.set(instance, "JAVA");
    }


}
