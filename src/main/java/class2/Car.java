package class2;

public class Car {
    int speed;
    String brand;

    Car(String name) {
        speed = 0;
        brand = name;
    }

    void speedUp(int howMuch) {
        int speedAfterAcceleration = speed + howMuch;
        speed = speedAfterAcceleration;
    }

    int showMeTheVelocity() {
        return speed;
    }

    String giveMeBrand() {
        return brand;
    }

    void changeBrand(String newBrand) {
        brand = newBrand;
    }

}
