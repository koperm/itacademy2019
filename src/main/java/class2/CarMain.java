package class2;

public class CarMain {
    public static void main(String[] args) {
        Car myFirstCar = new Car("Ferrari");
        Car mySecondCar = new Car("Ford");

        myFirstCar.speedUp(50);
        mySecondCar.brand = "Mini";
        mySecondCar.changeBrand("AgainFord");
    }
}
