package class13.multiinheritance.v1;

public class Dell extends AbstractServer {
    @Override
    public int getG3dMark() {
        return 789;
    }

    @Override
    public int getMHz() {
        return getNrOfProcessors() * 333;   // TODO because we extend AbstractServer we have multiple CPUs
    }

    @Override
    public int getPrice() {
        return 5000;    // TODO it declares price for the whole computer, not the CPU or GPU itself
    }
}
