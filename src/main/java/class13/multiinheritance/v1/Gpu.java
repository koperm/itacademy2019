package class13.multiinheritance.v1;

public interface Gpu {
    int getG3dMark();

    int getPrice();
}
