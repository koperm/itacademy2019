package class13.multiinheritance.v1;

public abstract class AbstractServer implements Computer {

    int getNrOfProcessors() {
        System.out.println("Doubling processor clock for " + this.getClass().getSimpleName());
        return 2;
    }
}
