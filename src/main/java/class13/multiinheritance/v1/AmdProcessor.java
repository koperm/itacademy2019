package class13.multiinheritance.v1;

public class AmdProcessor implements Processor {

    @Override
    public int getMHz() {
        return 200;
    }

    @Override
    public int getPrice() {
        return 250;
    }
}
