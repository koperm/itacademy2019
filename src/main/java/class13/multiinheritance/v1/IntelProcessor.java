package class13.multiinheritance.v1;

public class IntelProcessor implements Processor {

    @Override
    public int getMHz() {
        return 400;
    }

    @Override
    public int getPrice() {
        return 600;
    }
}
