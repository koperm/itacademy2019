package class13.multiinheritance.v1;

public class MainClass {
    public static void main(String[] args) {
        Asus asus = new Asus();
        asus.printBasicInfo();
        asus.printCpuInfo();
//        asus.convertToGHz(asus.getMHz()); // TODO another corner case. You are not able to dereference static method from interface through reference.
        Processor.convertToGHz(asus.getMHz());

        System.out.println();
        Dell dell = new Dell();
        dell.printBasicInfo();
        dell.printCpuInfo();
    }
}
