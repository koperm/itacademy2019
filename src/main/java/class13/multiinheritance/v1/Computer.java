package class13.multiinheritance.v1;

public interface Computer extends Gpu, Processor {

    default void printBasicInfo() {
        System.out.println("Basic info for " + this.getClass().getSimpleName() +
                ". GPU: " + getG3dMark() + ". CPU: " + getMHz() +
                ". Whole computer price: " + getPrice());
    }
}
