package class13.multiinheritance.v1;

public class Asus implements Computer {
    @Override
    public int getMHz() {
        return 456;
    }

    @Override
    public int getG3dMark() {
        return 123;
    }

    @Override
    public int getPrice() {
        return 1200;    // TODO it declares price for the whole computer, not the CPU or GPU itself
    }
}
