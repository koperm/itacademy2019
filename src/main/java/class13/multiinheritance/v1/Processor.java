package class13.multiinheritance.v1;

public interface Processor {

    int getMHz();

    int getPrice();

    default void printCpuInfo() {
        System.out.println("Processor{" +
                "mHz=" + getMHz() +
                '}');
    }

    static int convertToGHz(int mhz) {
        return mhz / 1000;
    }
}
