package class13.multiinheritance.v2;

public class Impl1 implements Int1 {

    public void int1Method() {   // TODO do I have to implement this method?
        System.out.println("Implements Impl1.int1Method");
    }

    public void intDefaultMethod() {    // TODO do I have to override this method? try to comment out and run
        System.out.println("Overrides Impl1.intDefaultMethod");
    }

    public static void main(String[] args) {
        Impl1 impl1 = new Impl1();
        impl1.int1Method();
        impl1.intDefaultMethod();
//        impl1.intStaticMethod();  // TODO can you explain why its forbidden?
        Int1.intStaticMethod();
    }
}
