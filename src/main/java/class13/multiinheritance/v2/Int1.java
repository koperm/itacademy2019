package class13.multiinheritance.v2;

public interface Int1 {
    void int1Method();

    default void intDefaultMethod(){
        System.out.println("intDefaultMethod");
    }

    static void intStaticMethod(){
        System.out.println("Interface static method");
    }
}
