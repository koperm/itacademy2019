package class13.multiinheritance.v2;

public abstract class AbstractImpl1 implements Int1{

    // TODO do I have to override or implement any of those method?
    public void int1Method() {
        System.out.println("Implements AbstractImpl1.int1Method");
    }

    public void intDefaultMethod() {
        System.out.println("Overrides AbstractImpl1.intDefaultMethod");
    }

    public static void main(String[] args) {
        Int1.intStaticMethod();
//        AbstractImpl1 abstractImpl1 = new AbstractImpl1();    // TODO why this is forbidden?
    }
}
