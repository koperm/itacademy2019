package class13.multiinheritance.v2;

public class Impl2 implements Int2 {
    public void int1Method() {  // TODO do I have to implement this method?
        System.out.println("Implements Int2Impl.int1Method");
    }

    public void int2Method() {  // TODO do I have to implement this method?
        System.out.println("Implements Int2Impl.int2Method");
    }

    public static void main(String[] args) {
        Impl1 impl1 = new Impl1();
        impl1.int1Method();
        impl1.intDefaultMethod();
//        intStaticMethod();    // TODO
    }
}
