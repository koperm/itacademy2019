package class13.multiinheritance.v2;

public class Impl3And4 implements Int3, Int4 {  // TODO why multiple inheritance of interfaces is possible?
    @Override
    public void int3And4Method() {  // TODO why there is only one implementation despite fact this method exist in both interfaces (Int3 and Int4?
        System.out.println("Impl3And4.int3And4Method");
    }

    public static void main(String[] args) {
        Impl3And4 impl3And4 = new Impl3And4();
        impl3And4.int3And4Method();
    }
}
