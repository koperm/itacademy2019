package class9.inheritance;

public class MainClass {
    public static void main(String[] args) {
//        Animal animal = new Animal(); // TODO why?
        Dog dog = new Dog();
        dog.eat("dog's food");

        Cat cat = new Cat();
        cat.eat("cat's food");
        cat.doNothing();

        Animal ref = cat;
        ref.eat("ref eat");
        cat.eat("cat eat");
//        ref.doNothing();
        ref = dog;
        ref.eat("dog eat");

///////////////////////////////////////////////////////////////////////////////

        Bulldog bulldog = new Bulldog();
        Dog dog1 = bulldog;
        Animal animal = bulldog;

        bulldog.eat("cats");
        dog1.eat("cats");
        animal.eat("cats");

///////////////////////////////////////////////////////////////////////////////

        feed(new Dog());
        feed(new Cat());
        feed(new Bulldog());
    }

    static void feed(Animal an) {
        an.eat("dynamic linking");
    }
}
