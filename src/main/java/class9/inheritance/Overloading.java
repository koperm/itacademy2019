package class9.inheritance;

public class Overloading {

//    void print(Object object) {   // TODO explain after inheritance class
//        System.out.println(object);
//    }

    void print(String string) {
        System.out.println(string);
    }

    void print(int integral) {
        System.out.println(integral);
    }

//    boolean print(int integral) { // TODO why?
//        System.out.println(integral);
//        return true;
//    }

    void print(String str, int integral) {
        System.out.println(integral);
    }

    void print(int integral, String str) {
        System.out.println(integral);
    }

    public static void main(String[] args) {
        Overloading overloading = new Overloading();
        overloading.print(4565);
        overloading.print("sadkfjsd");
        overloading.print("asdkf", 54);
        overloading.print(5458, "sdfad");
//        overloading.print(new StaticClass(456));

       overloading.print(546);
    }
}
