package class9.inheritance;

public class Cat implements Animal {

    @Override
    public void eat(String food) {
        System.out.println("Cat eats " + food);
    }

    void doNothing(){
        System.out.println("I am doing nothing");
    }
}
