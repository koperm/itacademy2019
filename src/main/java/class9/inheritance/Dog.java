package class9.inheritance;

public class Dog implements Animal{

    @Override
    public void eat(String food) {
        System.out.println("Dog eats " + food);
    }


}
