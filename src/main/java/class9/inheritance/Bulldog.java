package class9.inheritance;

public class Bulldog extends Dog {

    @Override
    public void eat(String food) {
        System.out.println("Bulldog eats" + food);
    }
}
