package class9.inheritance;

public interface Animal {

    void eat(String food);
}
