package class8.staticfinal;

public class FinalClass {
    final int x;

    public FinalClass(int age) {
        x = age;
    }

    void method1(final int y) {
//        x = 23;
//        y = 45;

        final int z = x;
//        z = 78;

        int x = 78;

        int f = x;
    }

    void printTaxLevel() {
        int taxLevel = StaticClass.getTaxLevel();
        taxLevel = 88;
        System.out.println(taxLevel);

        StaticClass ref = new StaticClass(54);
        ref.getTaxLevel();
    }

}
