package class8.staticfinal;

public class StaticClass {
    private static int taxLevel = 19;
    static int income = 45;
    String employer;

    final static String CONSTANT = "horror";
    private final static float PI_ROUND = 3.1415F;

    {
        employer = "bad guy";
    }

    static {
        income = 45;
        int i = 489;
        income = i;
    }

    public StaticClass(int income) {
        this.income = income;
        StaticClass.income = income;
    }

    public static int getTaxLevel() {
//        System.out.println(employer);
        return taxLevel;
    }

    public static void main(String[] args) {
        StaticClass one = new StaticClass(45);
        StaticClass two = new StaticClass(78);
        int lvl = StaticClass.getTaxLevel();
    }
}
