package class8.staticfinal;

public class Const {
    public static final String STRING = "some constant value";
    public static final float PI = 3.1415F;
}
