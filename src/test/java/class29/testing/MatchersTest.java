package class29.testing;

import class26.serialization.json.Car;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.util.Objects;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MatchersTest {

    @Test
    public void testConcatenate() {
        // given
        String expectedValue = "onetwo";

        // when
        TestedClass myUnit = new TestedClass();
        String actualValue = myUnit.concatenate("one", "two");

        // then
        assertThat(actualValue).isEqualToIgnoringCase(expectedValue);
        assertThat(actualValue).doesNotContainAnyWhitespaces();
        assertThat(actualValue).isNotNull().isNotBlank();
    }

    @Test
    public void testGetList() {
        // given
        List<String> expectedValue = List.of("one", "two");

        // when
        TestedClass myUnit = new TestedClass();
        List<String> actualValue = myUnit.getList();

        // then
        assertThat(actualValue).containsOnlyOnce("one", "two");
        assertThat(actualValue).contains("one").isSortedAccordingTo(Comparator.naturalOrder());
        assertThat(actualValue).as("This is a description of the test").doesNotContain("three");
        assertThat(actualValue).containsAll(expectedValue);
        assertThat(actualValue).extracting("length").contains(3, 3);

    }

    @Test
    public void testGetCar() {
        // given
        Car expectedValue = new Car();

        // when
        TestedClass myUnit = new TestedClass();
        Car actualValue = myUnit.getCar();

        // then
        SoftAssertions soft = new SoftAssertions();

        soft.assertThat(actualValue).isEqualToComparingFieldByFieldRecursively(expectedValue);
        soft.assertThat(actualValue).isEqualToComparingOnlyGivenFields(expectedValue, "color");
        soft.assertThat(actualValue).isEqualTo("black");

        soft.assertAll();
    }

    @Test
    public void testGetCarWithCustomAssert() {
        // when
        TestedClass myUnit = new TestedClass();
        Car actualValue = myUnit.getCar();

        // then
        CustomAssert.assertThat(actualValue).hasBlackColour();
    }

    static class CustomAssert extends AbstractAssert<CustomAssert, Car> {

        public CustomAssert hasBlackColour() {
            isNotNull();
            String errorMsg = "\nExpecting Car to have color <%s>\n but was:\n  <%s>";

            String actualColor = null;//actual.getColor();
            if (!Objects.areEqual(actualColor, "black")) {
                failWithMessage(errorMsg, "black", actualColor);
            }

            return this;
        }

        private CustomAssert(Car actual) {
            super(actual, CustomAssert.class);
        }

        public static CustomAssert assertThat(Car actual) {
            return new CustomAssert(actual);
        }

    }
}
