package class29.testing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junitpioneer.jupiter.TempDirectory;

import java.io.File;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(TempDirectory.class)
class JUnitExtensionTest {

    @Test
    void extensionTest(@TempDirectory.TempDir Path tempDir) {
        // given
        File localPublishDir = tempDir.toFile();

        // then
        assertNotNull(localPublishDir);
    }

}