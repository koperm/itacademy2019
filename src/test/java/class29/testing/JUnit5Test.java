package class29.testing;

import class26.serialization.json.Car;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JUnit5Test {

    @BeforeEach
    void beforeEach() {
        System.out.println("before each log");
    }

    @AfterEach
    void afterEach() {
        System.out.println("after each log");
    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("before All log");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("after All log");
    }

    private String getMeOneTwo() {
        return "onetwo";
    }

    @Test
    public void testConcatenate() {
        // given
        String expectedValue = getMeOneTwo();

        // when
        String actualValue = new TestedClass().concatenate("one", "two");

        // then
        assertEquals(expectedValue, actualValue);
        assertTrue(expectedValue.equals(actualValue));
    }

    @Test
    public void testThrowingFunction() {
        // given
        Class<RuntimeException> expectedException = RuntimeException.class;

        // when, then
        assertThrows(expectedException, () -> new TestedClass().throwingFunction());
//        assertThrows(expectedException, new TestedClass()::throwingFunction);
    }

    @Test
    public void testConcatenateWithNull() {
        // given
        String expectedValue = "onenull";

        // when
        TestedClass myUnit = new TestedClass();
        String actualValue = myUnit.concatenate("one", null);

        // then
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(actualValue).isEqualTo("");
        softAssertions.assertThat(actualValue).isEqualTo(expectedValue);
        softAssertions.assertAll();

        assertEquals(expectedValue, actualValue);
        assertTrue(expectedValue.equals("")); // TODO compare with soft assertions
        System.out.println("assertSoftly");
        assertTrue(expectedValue.equals(actualValue));
    }

    @Test
    public void testConcatenateWithSoftAssertions() {
        // given
        String expectedValue = "onenull";

        // when
        TestedClass myUnit = new TestedClass();
        String actualValue = myUnit.concatenate("one", null);

        // then
        assertAll(
                () -> assertEquals("", actualValue),
                () -> assertEquals(expectedValue, actualValue),
                () -> assertTrue(expectedValue.equals(actualValue))
        );
    }

    private static Stream<Arguments> dataInput() {
        return Stream.of(
                Arguments.of("one", "two"),
                Arguments.of(null, "two"),
                Arguments.of("one", null),
                Arguments.of(null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("dataInput")
    public void testConcatenateWithParameters(String first, String second) {
        // when
        TestedClass myUnit = new TestedClass();

        // then
        assertDoesNotThrow(() -> myUnit.concatenate(first, second));
    }

    private static Stream<Arguments> carDataInput() {
        return Stream.of(
                Arguments.of(new Car()),
                Arguments.of(new Car())
        );
    }

    @ParameterizedTest
    @MethodSource("carDataInput")
    void testWithExplicitArgumentConversion(@ConvertWith(ToStringCarConverter.class) String argument) {
        assertNotNull(argument);
    }

    private static class ToStringCarConverter extends SimpleArgumentConverter {
        @Override
        protected Object convert(Object source, Class<?> targetType) {
            assertEquals(Car.class, source.getClass(), "Can only convert from Car type.");
            assertEquals(String.class, targetType, "Can only convert to String");
            Car car = (Car) source;
            return "My car is in " + car + " color";
        }
    }

    @Test
    public void testTimeout() {
        // when, then
        TestedClass myUnit = new TestedClass();
        List<String> gotList = assertTimeout(Duration.ofSeconds(2), myUnit::getList);
        assertTrue(gotList.size() == 2);
    }
}
