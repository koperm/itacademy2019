package class29.testing.mocks;

import class29.testing.DependencyClass;
import class29.testing.TestedClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MockingTestWithAnnotations {

    @Mock
    DependencyClass dependencyMock;

    // TODO if you do not use ExtendWith annotation
//    @BeforeEach
//    void setUp() {
//        dependencyMock = mock(DependencyClass.class);
//        MockitoAnnotations.initMocks(this);
//    }

    @Test
    public void testWhenThen() {
        // given
        when(dependencyMock.getAttr())
                .thenReturn("first return")
                .thenReturn("sec return");

        // given, then
        assertDoesNotThrow(() -> new TestedClass().methodWithDependency(dependencyMock));
    }

    @Test
    public void testDoNothing() {
        // given
        doNothing().when(dependencyMock).setAttr(anyString());

        // when, then
        assertDoesNotThrow(() -> new TestedClass().methodWithDependency(dependencyMock));
    }

    @Test
    public void testThrowing() {
        // given
        when(dependencyMock.getAttr())
                .thenThrow(new RuntimeException());

        // when, then
        assertThrows(RuntimeException.class, () -> new TestedClass().methodWithDependency(dependencyMock));
    }

    @Test
    public void testDoAnswer() {
        // given
        doAnswer(invocation -> {
            System.out.println("I am implementing a body");
            return null;
        }).when(dependencyMock).getAttr();

        // when, then
        TestedClass myUnit = new TestedClass();
        assertDoesNotThrow(() -> myUnit.methodWithDependency(dependencyMock));
    }

    @Test
    public void verifyTest() {
        // given
        doNothing().when(dependencyMock).setAttr(anyString());

        // when, then
        TestedClass myUnit = new TestedClass();
        assertDoesNotThrow(() -> myUnit.methodWithDependency(dependencyMock));
        verify(dependencyMock, times(1)).getAttr();
    }

    @Test
    public void verifyTest1() {
        // given
        doNothing().when(dependencyMock).setAttr(anyString());

        // when, then
        TestedClass myUnit = new TestedClass();
        String actualValue = assertDoesNotThrow(() -> myUnit.methodWithDependency(dependencyMock));
        verify(dependencyMock, times(1)).getAttr(); // never()/atLeastOnce()/atLeast(int)/atMost(int)
        verify(dependencyMock, times(1)).setAttr(eq("new attr"));
        assertEquals(actualValue, "new attr");
    }

}
