package class29.testing.mocks;

import class29.testing.DependencyClass;
import class29.testing.TestedClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SpyingTestWithAnnotations {

//    @Spy
//    private DependencyClass dependencyMock = new DependencyClass("initial attr"); // the same as below

    private DependencyClass dependencyMock;

    @BeforeEach
    void init() {
        dependencyMock = spy(new DependencyClass("initial attr"));
    }

    @Test
    public void testWhenThen() {
        // given
        doReturn("first return").when(dependencyMock).getAttr();

        // given, then
        assertDoesNotThrow(() -> new TestedClass().methodWithDependency(dependencyMock));
    }

    @Test
    public void testDoNothing() {
        // given
        doNothing().when(dependencyMock).setAttr(anyString());  // TODO comment it and run

        // when, then
        assertDoesNotThrow(() -> new TestedClass().methodWithDependency(dependencyMock));
    }

    @Test
    public void testThrowing() {
        // given
        when(dependencyMock.getAttr()).thenThrow(new RuntimeException());

        // when, then
        assertThrows(RuntimeException.class, () -> new TestedClass().methodWithDependency(dependencyMock));
    }

    @Test
    public void testDoAnswer() {
        // given
        doAnswer(invocation -> {
            System.out.println("I am implementing a body");
            return null;
        }).when(dependencyMock).getAttr();

        // when, then
        TestedClass myUnit = new TestedClass();
        assertDoesNotThrow(() -> myUnit.methodWithDependency(dependencyMock));
    }

    @Test
    public void verifyTest() {
        // given
        doNothing().when(dependencyMock).setAttr(anyString());

        // when, then
        TestedClass myUnit = new TestedClass();
        assertDoesNotThrow(() -> myUnit.methodWithDependency(dependencyMock));
        verify(dependencyMock, times(1)).getAttr();
    }

}
