package class29.testing.mocks;

import class29.testing.DependencyClass;
import class29.testing.TestedClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CaptorTest {

    @Captor
    private ArgumentCaptor<String> captor;

    @Mock
    DependencyClass dependencyMock;

    @Test
    public void testWhenThen() {
        // given
        doNothing().when(dependencyMock).setAttr(any());

        // given, then
        assertDoesNotThrow(() -> new TestedClass().methodWithDependency(dependencyMock));
        verify(dependencyMock).setAttr(captor.capture());

        assertEquals(captor.getValue(), null);
    }
}
