package class29.testing.mocks;

import class29.testing.DependencyClass;
import class29.testing.TestedClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;

public class MockingTest {

    @Test
    public void testDep() {
        // given
        DependencyClass dependencyMock = mock(DependencyClass.class);

        // when
        TestedClass myUnit = new TestedClass();

        // then
        assertDoesNotThrow(() -> myUnit.methodWithDependency(dependencyMock));
    }
}
